import './home.scss';

// jQuery
import $ from 'jquery';
window.jQuery = $;
window.$ = $;

import Popper from 'popper.js';
window.Popper = Popper;

// Bootstrap
import 'bootstrap';
$(".card-body").hide();
$("#nosOffres .reduire").hide();

$(function(){
  $(".navbar a, .slogan a, footer a").click(function(event){
    event.preventDefault();
    var hash = this.hash;
    $('body,html').animate({scrollTop: $(hash).offset().top} , 900 , function(){window.location.hash = hash;});
  });
  $("#nosRealisations .card a").click(function(event){
    $('#body'+this.id).fadeIn();
    $("#"+this.id).hide();
  });
  $("#nosRealisations .card .upButton").click(function(event){
    $('#body'+this.id).fadeOut();
    $("#"+this.id).show();
  });

  $("#nosOffres .container div div .enSavoirPlus").click(function (event) {
    $(".offre1").css("filter", "blur(4px)");
    $(".offre2").css("filter", "blur(4px)");
    $(".offre3").css("filter", "blur(4px)");
    $(".offre"+this.id).css("filter", "blur(0)");
    $("#"+this.id).hide();
    $("#r"+this.id).show();
    $(".offre"+this.id).css({"background" : "url('/assets/img/bigOffre.png')", "background-repeat": "no-repeat;"});

    console.log("offre"+this.id);
  });

  $(".reduire").click(function(event){
    $(".offre1").css("filter", "blur(0)");
    $(".offre2").css("filter", "blur(0)");
    $(".offre3").css("filter", "blur(0)");
    $("#"+this.id.charAt(1)).show();
    $("#"+this.id).hide();
    $(".offre"+this.id.charAt(1)).css({"background" : "no-repeat url('/assets/img/offre.png')", "background-repeat": "no-repeat;"});
    // $(".offre"+this.id.charAt(1)).css("background-repeat, no-repeat;");

  });

})

// var home = {
//     init: function () {
//         console.log('home');
//     }
// };




// end (document)
