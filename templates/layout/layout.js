import './layout.scss';

// jQuery
import $ from 'jquery';
window.jQuery = $;
window.$ = $;

import Popper from 'popper.js';
window.Popper = Popper;

// Bootstrap
import 'bootstrap';


// DataTables
import 'datatables.net';
import 'datatables.net-bs4';
import 'datatables.net-responsive-bs4';

require('datatables.net-bs4/css/dataTables.bootstrap4.css');
require('datatables.net-responsive-bs4/css/responsive.bootstrap4.css');
