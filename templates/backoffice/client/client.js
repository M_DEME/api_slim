var list = {
    xToken: $('meta[http-equiv="X-Token"]').attr('content'),
    init: function () {
        list.displayTable();
    },
    displayTable: function () {
        /*var $table = $('#listOfQuestions').DataTable();
        if ($table) {
            $table.destroy();
        }*/
        var $table = $('#listOfClients').DataTable(
            {
                responsive: true,
                processing: true,
                //serverSide: true,
                searching: true,
                ordering: true,
                paging: true,
                ajax: {
                    url: 'api/client',
                    type: 'GET',
                    'headers': {
                        'X-Token': list.xToken,
                        'Content-Type': 'application/json'
                    },
                },
                lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
                columnDefs: [
                    {
                        searchable: true,
                        orderable: true,
                        data: 'client_first_name',
                        //width: '5%',
                        targets: 0
                    },
                    {
                        searchable: true,
                        orderable: true,
                        data: 'client_last_name',
                        //width: '10%',
                        targets: 1
                    },
                    {
                        searchable: true,
                        orderable: true,
                        data: 'client_email',
                        //width: '10%',
                        targets: 2
                    },
                    {
                        searchable: false,
                        orderable: false,
                        data: null,
                        width: '10%',
                        render: function (data, type, row, meta) {
                            return `<div class="btn-group flex-nowrap"
                                        role="group"
                                        aria-label="Row Button Bar">
                                        <button type="button"
                                            class="btn btn-secondary"
                                            data-toggle="tooltip"
                                            title="Edit the question"
                                            value="${data.clent_index}"
                                            name="edit"
                                        >&#10149;</button>
                                </div>`;
                        },
                        targets: 3
                    }
                ],
                order: [[0, 'desc']]
            }); // end DataTable()

        /*$table.on('click', 'button', function () {
            if (this.name.localeCompare('edit') === 0) {
                //console.log('Button Table game_index:' + this.id + '-href:' + window.location.hostname);
                window.open('/question/' + this.id, '_self');
            } else {
                // Send data
                $.ajax({
                    type: 'DELETE',
                    //the url where you want to sent the userName and password to
                    url: '/question/' + this.id,
                    headers: {
                        'X-Token': list.xToken,
                    },
                    contentType: 'application/json',
                    //json object to sent to the authentication url
                    success: function (result, textStatus, jqxhr) {
                        switch (textStatus) {
                            case 'success':
                                // HTTP 201 OK
                                console.log('Status 201 - result:');
                                console.log(result);
                                break;
                            case 'nocontent':
                                // HTTP 204 OK
                                //console.log('Status 204: nothing to do!');
                                //question.fill();
                                window.open(window.location.href, '_self');
                                break;
                            default:
                                var status = textStatus + ', ' + result + ', ' + jqxhr;
                                console.log('Request success: ' + status);
                        }
                    },
                    error: function (jqxhr, textStatus, error) {
                        var err = textStatus + ', ' + error + ', ' + jqxhr;
                        console.log('Request Failed: ' + err);
                    }
                });
            }
            return false;
        }).on('error.dt', function (e, settings, techNote, message) {
            //console.log('An error has been reported by DataTables: ', message);
            //console.log(e);

            if (message.localeCompare('Ajax error')) {
                //console.log(message);
                window.open('/login', '_self');
            }
        });*/
        // data-toggle="tooltip" data-placement="top" title="Tooltip on top
        /*$('input').attr('data-toggle', 'tooltip').attr('data-placement', 'top')
            .attr('title', 'Pour faire une recherche:');*/

        /*$('input').popover({
            html: true,
            title: `<b>Recherche</b>
                <br/>title = <b>"</b>critère de recherche<b>"</b>
                <br/>tag = <b>"</b>tag recherché<b>"</b>
                <br/>theme = <b>"</b>theme recherché<b>"</b>
                <br/>Pour en sélectionner plusieurs:
                <br/> opérateur possible: and ou or
                <br/>ex: tag="comptage" and theme="lac"`,
            placement: 'bottom'
        });*/
    }
};

$(document).ready(list.init); // end (document)
