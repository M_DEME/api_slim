<?php

declare(strict_types=1);

namespace BO\Helper;

use UnexpectedValueException;
use Selective\ArrayReader\ArrayReader;
use Firebase\JWT\JWT;
use BO\Domain\Data\accountData;

final class AuthenticationHelper
{
    private $tokenHeader;
    private $issuer;
    private $secret;

    private $account;

    public function __construct(
        array $settings
    ) {
        $data = new ArrayReader($settings);
        $this->tokenHeader = $data->findString('token_header', 'X-Token');
        $this->secret = $data->findString('secret', 'no_secret');
        $this->issuer = $data->findString('app_url', '');
    }

    public function generateToken(
        AccountData $account,
        string $duration = 'now +6 hours'
    ): string {
        $now = new \DateTime();
        $future = new \DateTime($duration);
        // $future = new \DateTime("now +5 min");
        $payload = [
            'iat' => $now->getTimeStamp(),
            'exp' => $future->getTimeStamp(),
            'jti' => base64_encode(random_bytes(16)),
            'iss' => $this->issuer,
            'sub' => [
                'id' => hash_hmac('sha256', "{$account->index}", $account->apiKey),
                'email' => $account->email,
                'ip' => $account->ip,
                'ua' => $account->userAgent,
            ],
        ];

        return JWT::encode($payload, $this->secret);
    }

    public function decodeToken($jwt): object
    {
        try {
            $status = JWT::decode($jwt, $this->secret, [
                'HS256',
            ]);
        } catch (\Firebase\JWT\ExpiredException $e) {
            $status = new \stdClass();
        }

        return $status;
    }

    /*public function isAdmin(): bool
    {
        if (isset($_SESSION['role'])) {
            $role = json_decode($_SESSION['role'], true);
            if (!empty($role)) {
                return $role['isAdmin'];
            }
        }

        return false;
    }*/
    public function getHeader(): string
    {
        return $this->tokenHeader;
    }

    /**
     * Set the identity into storage or null if no identity is available.
     *
     * @param AccountData
     */
    public function setAccount(AccountData $data): void
    {
        unset($data->password, $data->apiKey);
        $this->account = $data;
    }

    /**
     * Returns the identity from storage or null if no identity is available.
     *
     * @throws UnexpectedValueException
     *
     * @return AccountData The user
     */
    public function getAccount(): AccountData
    {
        if (!$this->account) {
            throw new UnexpectedValueException('No account available');
        }

        return $this->account;
    }
}
