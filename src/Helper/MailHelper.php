<?php

namespace BO\Helpers;

use Psr\Log\LoggerInterface;
use PHPMailer\PHPMailer\PHPMailer;

final class MailHelper
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function sendMail($address, $subject, $body, $isHtml = false)
    {
        $status = true;
        // Create a new PHPMailer instance
        $mail = new PHPMailer();
        $mail->CharSet = 'UTF-8';
        // Tell PHPMailer to use SMTP
        $mail->isSMTP();
        // Enable SMTP debugging
        // 0 = off (for production use)
        // 1 = client messages
        // 2 = client and server messages
        $mail->SMTPDebug = getenv('MAILER_SMTP_DEBUG');
        // Set the hostname of the mail server
        $mail->Host = getenv('MAILER_HOST');
        // Set the SMTP port number - likely to be 25, 465 or 587
        $mail->Port = getenv('MAILER_PORT');
        // Whether to use SMTP authentication
        $mail->SMTPAuth = getenv('MAILER_SMTP_AUTH');
        // Username to use for SMTP authentication
        $mail->Username = getenv('MAILER_USERNAME');
        // Password to use for SMTP authentication
        $mail->Password = getenv('MAILER_PASSWORD');

        // Set who the message is to be sent from
        $mail->setFrom(getenv('MAILER_USERNAME'));
        // Set who the message is to be sent to
        $mail->addAddress($address);
        // Set the subject line
        $mail->Subject = $subject;
        if ($isHtml) {
            $mail->isHTML(true);
            $mail->msgHTML($body);
        } else {
            // Replace the plain text body with one created manually
            $mail->Body = $body;
        }
        // send the message, check for errors
        if (!$mail->send()) {
            $this->logger->warning(
                'Mailer::sendMail() Error:' . $mail->ErrorInfo
            );
            $status = false;
        }

        return $status;
    }
}
