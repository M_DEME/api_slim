<?php

namespace BO\Action;

use Slim\Views\Twig;
use Slim\Routing\RouteContext;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Fig\Http\Message\StatusCodeInterface;
use BO\Forms\LoginForm;
use BO\Factory\LoggerFactory;
use BO\Domain\Service\AccountService;
use BO\Domain\Data\AccountData;

final class PostLoginAction
{
    /**
     * @var Twig
     */
    private $twig;
    private $service;
    private $loginForm;

    /**
     * The constructor.
     *
     * @param Twig $twig The twig engine
     */
    public function __construct(
        LoggerFactory $loggerFactory,
        Twig $twig,
        AccountService $accountService
    ) {
        $this->logger = $loggerFactory->createInstance('login.log', 'PostLoginAction');
        $this->twig = $twig;
        $this->service = $accountService;
        $this->loginForm = new LoginForm($this->logger, 'login', 'postLogin');
    }

    /**
     * Action.
     *
     * @param ServerRequest $request  The request
     * @param Response      $response The response
     *
     * @return ResponseInterface The response
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $this->loginForm->setRequest($request);

        if ($this->loginForm->isSubmitted() && !$this->loginForm->isValid()) {
            $this->logger->warning(
                'Id:' . $this->loginForm->getId() . ' is not valid...'
            );
        } else {
            $account = new AccountData([
                'email' => $this->loginForm->getEmail()->getValue(),
            ]);
            $account->ip = $request->getAttribute('client-ip');
            $account->userAgent = $request->getHeaderLine('User-Agent');

            $token = $this->service->loginForm(
                $account,
                $this->loginForm->getPassword()->getValue(),
                $this->loginForm->getToken()->getValue()
            );
        }
        $router = RouteContext::fromRequest($request)->getRouteParser();
        if (empty($token)) {
            $url = $router->urlFor('login');
        } else {
            $url = $router->urlFor(
                'boHome',
                [],
                [$this->service->authentication->getHeader() => $token]
            );
        }

        return $response->withHeader('Location', $url)->withStatus(StatusCodeInterface::STATUS_FOUND); // 302
    }
}
