<?php


namespace BO\Action\ApiTicket;



use BO\Domain\Data\TicketData;
use BO\Domain\Service\TicketService;
use BO\Factory\LoggerFactory;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Selective\Encoding\JsonEncoding;
use Slim\Views\Twig;

class CreationTicketAction
{
    private $logger;
    private $jsonEncoding;
    private $service;
    private $twig;
    public function __construct(
        LoggerFactory $loggerFactory,
        JsonEncoding $jsonEncoding,
        TicketService $ticketService,
        Twig $twig
    ) {
        $this->logger = $loggerFactory->createInstance('ticket.log', 'ListDatatableClientAction');
        $this->jsonEncoding = $jsonEncoding;
        $this->service = $ticketService;
        $this->twig = $twig;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {


        if (
            false === strstr(
                $request->getHeaderLine('Content-Type'),
                'application/json'
            )
        ) {
            return $response->withStatus(StatusCodeInterface::STATUS_UNSUPPORTED_MEDIA_TYPE);
        }
        $body = $request->getParsedBody();
        if (empty($body)) {
            return $response->withStatus(StatusCodeInterface::STATUS_NOT_ACCEPTABLE);
        }
        $this->logger->debug('CreateClientAction', ['body' => $body]);

        // $data = ['id' => 22, 'date' => '2020-04-22', 'texte' => 'vitobois', 'severite' => 'normal'];
        $ticket = new TicketData($body);
        var_dump($ticket);
        if ($this->service->create($ticket)) {
            $response->getBody()->write($this->jsonEncoding->encodeJson($ticket));

            return $response->withHeader('Content-Type', 'application/json')
                ->withStatus(StatusCodeInterface::STATUS_CREATED);
        }
        return $response->withStatus(StatusCodeInterface::STATUS_CONFLICT);
    }
}