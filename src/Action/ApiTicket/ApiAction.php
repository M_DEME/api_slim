<?php


namespace BO\Action\ApiTicket;


use BO\Domain\Service\AccountService;
use BO\Factory\LoggerFactory;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Views\Twig;

class ApiAction
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * The constructor.
     *
     * @param Twig $twig The twig engine
     */
    public function __construct(Twig $twig)
    {
        $this->twig = $twig;
    }

    /**
     * Action.
     *
     * @param ServerRequest $request  The request
     * @param Response      $response The response
     *
     * @return ResponseInterface The response
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        return $this->twig->render(
            $response,
            'apiTemplate/ApiPage.twig',
            [
                'html_title' => 'Bienvenue sur Vit\'o Bois',
                'meta_description' => 'La home page de Vit\'o Bois.',
            ]
        );
    }
}