<?php


namespace BO\Action\ApiTicket;


use BO\Domain\Service\ClientService;
use BO\Domain\Service\TicketService;
use BO\Factory\LoggerFactory;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Http\Response;
use Selective\Encoding\JsonEncoding;
use Slim\Views\Twig;

class ListAction
{
    private $logger;
    private $jsonEncoding;
    private $service;

    public function __construct(
        LoggerFactory $loggerFactory,
        JsonEncoding $jsonEncoding,
        TicketService $ticketService
    ) {
        $this->logger = $loggerFactory->createInstance('ticket.log', 'ListDatatableClientAction');
        $this->jsonEncoding = $jsonEncoding;
        $this->service = $ticketService;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
                    $list = [];
            $list = $this->service->readAll();
            //var_dump($list);
            $status = StatusCodeInterface::STATUS_CREATED;
        $data['data'] = $list;

        // Build the HTTP response
        $response->getBody()->write(
            $this->jsonEncoding->encodeJson($data)
        );
        $status = StatusCodeInterface::STATUS_CREATED;

        return $response->withHeader('Content-Type', 'application/json')
            ->withStatus($status)
            ;
    }
}