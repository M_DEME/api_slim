<?php

namespace BO\Action;

use Slim\Views\Twig;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use BO\Forms\LoginForm;
use BO\Factory\LoggerFactory;
use BO\Domain\Service\AccountService;

final class LoginAction
{
    /**
     * @var Twig
     */
    private $twig;
    private $service;
    private $loginForm;

    /**
     * The constructor.
     *
     * @param Twig $twig The twig engine
     */
    public function __construct(
        LoggerFactory $loggerFactory,
        AccountService $accountService,
        Twig $twig
    ) {
        $this->logger = $loggerFactory->createInstance('login.log', 'LoginPageAction');
        $this->twig = $twig;
        $this->service = $accountService;
        $this->loginForm = new LoginForm($this->logger, 'login', 'postLogin');
    }

    /**
     * Action.
     *
     * @param ServerRequest $request  The request
     * @param Response      $response The response
     *
     * @return ResponseInterface The response
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $this->service->logout();

        $this->loginForm->setToken(
            $this->service->getLoginToken(
                $request->getAttribute('client-ip'),
                $request->getHeaderLine('User-Agent')
            )
        );

        return $this->twig->render(
            $response,
            'login/loginPage.twig',
            [
                'html_title' => 'Bienvenue',
                'meta_description' => 'La page Login',
                'login_form' => $this->loginForm->getView($this->twig),
            ]
        );
    }
}
