<?php

namespace BO\Action\Backoffice\Client;

use Slim\Views\Twig;
use Selective\Encoding\JsonEncoding;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Fig\Http\Message\StatusCodeInterface;
use BO\Factory\LoggerFactory;
use BO\Domain\Service\ClientService;
use BO\Domain\Service\AccountService;
use BO\Domain\Data\ClientData;
use BO\Domain\Data\AccountData;

class ReadClientAction
{
    private $logger;
    private $service;
    private $twig;
    private $accountService;

    public function __construct(
        LoggerFactory $loggerFactory,
        ClientService $clientService,
        AccountService $accountService,
        Twig $twig
    ) {
        $this->logger = $loggerFactory->createInstance('client.log', 'ReadClientAction');
        $this->service = $clientService;
        $this->twig = $twig;
        $this->accountService = $accountService;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        if (empty($request->getAttribute('token'))) {
            return $response->withStatus(StatusCodeInterface::STATUS_UNAUTHORIZED);
        }

        $id = urldecode($request->getAttribute('id'));
        $data['index'] = $request->getAttribute('token_account')->index;

        $accountData = new AccountData($data);

        $this->accountService->selectByIndex($accountData);

        $token = $request->getAttribute('token');
        $client = null;

        $client = new ClientData([]);
        $this->logger->debug('selectAll');
        $list = $this->service->selectAll($client);

        $this->logger->debug(
            'ReadClientAction',
            [
                'client' => $client,
            ]
        );

        if (empty($list)) {
            return $response->withStatus(StatusCodeInterface::STATUS_NO_CONTENT);
        }
        //$response->getBody()->write($this->jsonEncoding->encodeJson($list));
        $response->withHeader('Content-Type', 'application/json')->withStatus(StatusCodeInterface::STATUS_ACCEPTED);

        return $this->twig->render(
            $response,
            'backoffice/boListClient.twig',
            [
                'html_title' => 'List Clients',
                'active' => 1,
                'token' => $token,
                'meta_description' => 'La liste des clients du back-office.',
                'list' => $list,
                'date' => date('Y-m-d'),
                'email' => $accountData->email,
                'role' => $accountData->role,
                'avatar' => $accountData->avatar,
            ]
        );
    }
}
