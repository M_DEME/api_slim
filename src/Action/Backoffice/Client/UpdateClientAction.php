<?php


namespace BO\Action\Backoffice\Client;


use BO\Domain\Data\ClientData;
use BO\Domain\Service\ClientService;
use BO\Factory\LoggerFactory;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Selective\Encoding\JsonEncoding;

class UpdateClientAction
{
    private $logger;
    private $jsonEncoding;
    private $service;

    public function __construct(
        LoggerFactory $loggerFactory,
        JsonEncoding $jsonEncoding,
        ClientService $clientService
    ) {
        $this->logger = $loggerFactory->createInstance('client.log', 'UpdateclientAction');
        $this->jsonEncoding = $jsonEncoding;
        $this->service = $clientService;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        if (empty($request->getAttribute('token'))) {
            return $response->withStatus(StatusCodeInterface::STATUS_UNAUTHORIZED);
        }
        if (
            false === strstr(
                $request->getHeaderLine('Content-Type'),
                'application/json'
            )
        ) {
            return $response->withStatus(StatusCodeInterface::STATUS_UNSUPPORTED_MEDIA_TYPE);
        }
        $body = $request->getParsedBody();
        if (empty($body)) {
            return $response->withStatus(StatusCodeInterface::STATUS_NOT_ACCEPTABLE);
        }

        $client = new ClientData($body);
        $client->index = $request->getAttribute('index');


        if ($this->service->updateClient($client)){
            $response->getBody()->write($this->jsonEncoding->encodeJson($client));
            return $response->withHeader('Content-Type', 'application/json')
                ->withStatus(StatusCodeInterface::STATUS_CREATED);
        }
        return $response->withStatus(StatusCodeInterface::STATUS_CONFLICT);
    }
}