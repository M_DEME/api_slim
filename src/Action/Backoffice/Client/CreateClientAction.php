<?php


namespace BO\Action\Backoffice\Client;

use BO\Domain\Data\ClientData;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Selective\Encoding\JsonEncoding;
use Slim\Views\Twig;
use BO\Domain\Service\ClientService;
use BO\Factory\LoggerFactory;


class CreateClientAction
{
    private $logger;
    private $twig;
    private $service;
    private $jsonEncoding;

    public function __construct(
        LoggerFactory $loggerFactory,
        Twig $twig,
        JsonEncoding $jsonEncoding,
        ClientService $service
    ){
        $this->logger = $loggerFactory->createInstance('client.log', 'CreateClientAction');
        $this->twig = $twig;
        $this->service = $service;
        $this->jsonEncoding = $jsonEncoding;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ):ResponseInterface{
//        if (empty($request->getAttribute('token'))) {
//            return $response->withStatus(StatusCodeInterface::STATUS_UNAUTHORIZED);
//        }
        if (
            false === strstr(
                $request->getHeaderLine('Content-Type'),
                'application/json'
            )
        ) {
            return $response->withStatus(StatusCodeInterface::STATUS_UNSUPPORTED_MEDIA_TYPE);
        }
        $body = $request->getParsedBody();
        if (empty($body)) {
            return $response->withStatus(StatusCodeInterface::STATUS_NOT_ACCEPTABLE);
        }
        $this->logger->debug('CreateClientAction', ['body' => $body]);

        $client = new ClientData($body);
        print_r($client);
        if ($this->service->create($client)) {
            $response->getBody()->write($this->jsonEncoding->encodeJson($client));

            return $response->withHeader('Content-Type', 'application/json')
                ->withStatus(StatusCodeInterface::STATUS_CREATED);
        }

        return $response->withStatus(StatusCodeInterface::STATUS_CONFLICT);
}
}