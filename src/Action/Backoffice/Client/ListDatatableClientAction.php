<?php

namespace BO\Action\Backoffice\Client;

use Selective\Encoding\JsonEncoding;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Fig\Http\Message\StatusCodeInterface;
use BO\Factory\LoggerFactory;
use BO\Domain\Service\ClientService;

class ListDatatableClientAction
{
    private $logger;
    private $jsonEncoding;
    private $service;

    public function __construct(
        LoggerFactory $loggerFactory,
        JsonEncoding $jsonEncoding,
        ClientService $clientService
    ) {
        $this->logger = $loggerFactory->createInstance('client.log', 'ListDatatableClientAction');
        $this->jsonEncoding = $jsonEncoding;
        $this->service = $clientService;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        if (empty($request->getAttribute('token'))) {
            $list = [];
            $status = StatusCodeInterface::STATUS_UNAUTHORIZED;
        } else {
            $list = $this->service->selectAll();
            $status = StatusCodeInterface::STATUS_CREATED;
        }
        $data['data'] = $list;

        // Build the HTTP response
        $response->getBody()->write(
            $this->jsonEncoding->encodeJson($data)
        );

        return $response->withHeader('Content-Type', 'application/json')
            ->withStatus($status)
        ;
    }
}
