<?php

namespace BO\Action\Backoffice\Client;

use Slim\Views\Twig;
use Selective\Encoding\JsonEncoding;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Fig\Http\Message\StatusCodeInterface;
use BO\Forms\ClientForm;
use BO\Factory\LoggerFactory;
use BO\Domain\Service\ClientService;
use BO\Domain\Data\ClientData;

class PostClientAction
{
    private $logger;
    private $twig;
    private $jsonEncoding;
    private $service;
    private $clientForm;

    public function __construct(
        LoggerFactory $loggerFactory,
        Twig $twig,
        JsonEncoding $jsonEncoding,
        ClientService $clientService
    ) {
        $this->logger = $loggerFactory->createInstance('client.log', 'UpdateclientAction');
        $this->twig = $twig;
        $this->jsonEncoding = $jsonEncoding;
        $this->service = $clientService;
        $this->clientForm = new ClientForm($this->logger, 'client', 'postClient', 'forms/client_form.twig');
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $this->clientForm->setRequest($request);

        if ($this->clientForm->isSubmitted() && !$this->clientForm->isValid()) {
            $this->logger->warning(
                'Id:' . $this->clientForm->getId() . ' is not valid...'
            );
        } else {
            $client = new ClientData([
                'lastName' => $this->clientForm->getLastNameField()->getValue(),
                'firstName' => $this->clientForm->getLastNameField()->getValue(),
                'email' => $this->clientForm->getEmailField()->getValue(),
            ]);
            $this->logger->debug('', ['client' => $client]);
            if ($this->service->create($client)) {
                return $response->withHeader('Content-Type', 'application/json')
                    ->withStatus(StatusCodeInterface::STATUS_NO_CONTENT)
                ;
            }
            $this->clientForm->getEmailField()->setValidToFalse();
            $this->clientForm->getEmailField()->setErrorMessage('L\'email est déjà créé chez nous !');
        }

        /*$body = $request->getParsedBody();
        if (empty($body)) {
            return $response->withStatus(StatusCodeInterface::STATUS_NOT_ACCEPTABLE);
        }

        $client = new ClientData($body);
        $client->index = $request->getAttribute('index');


        if ($this->service->updateClient($client)){
            $response->getBody()->write($this->jsonEncoding->encodeJson($client));
            return $response->withHeader('Content-Type', 'application/json')
                ->withStatus(StatusCodeInterface::STATUS_CREATED);
        }*/
        return $this->twig->render(
            $response,
            'backoffice/boClientPage.twig',
            [
                'html_title' => 'Client',
                'active' => 1,
                'token' => $request->getAttribute('token'),
                'meta_description' => 'La home page du back-office.',
                'account' => $request->getAttribute('token_account'),
                'client_form' => $this->clientForm->getView($this->twig),
            ]
        );
    }
}
