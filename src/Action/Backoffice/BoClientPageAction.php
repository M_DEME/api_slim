<?php

namespace BO\Action\Backoffice;

use Slim\Views\Twig;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use BO\Forms\ClientForm;
use BO\Factory\LoggerFactory;
use BO\Domain\Service\AccountService;

final class BoClientPageAction
{
    /**
     * @var Twig
     */
    private $twig;
    private $service;
    private $clientForm;

    /**
     * The constructor.
     *
     * @param Twig $twig The twig engine
     */
    public function __construct(
        LoggerFactory $loggerFactory,
        Twig $twig,
        AccountService $accountService
    ) {
        $this->logger = $loggerFactory->createInstance('backoffice.log', 'BoClientPageAction');
        $this->twig = $twig;
        $this->service = $accountService;
        $this->clientForm = new ClientForm($this->logger, 'client', 'postClient', 'forms/client_form.twig');
    }

    /**
     * Action.
     *
     * @param ServerRequest $request  The request
     * @param Response      $response The response
     *
     * @return ResponseInterface The response
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $token = $request->getAttribute('token');

        return $this->twig->render(
            $response,
            'backoffice/boClientPage.twig',
            [
                'html_title' => 'Client',
                'active' => 1,
                'token' => $token,
                'meta_description' => 'La home page du back-office.',
                'account' => $request->getAttribute('token_account'),
                'client_form' => $this->clientForm->getView($this->twig),
            ]
        );
    }
}
