<?php

namespace BO\Action\Backoffice;

use Slim\Views\Twig;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use BO\Factory\LoggerFactory;
use BO\Domain\Service\AccountService;
use BO\Domain\Data\AccountData;

final class BoHomePageAction
{
    /**
     * @var Twig
     */
    private $twig;
    private $service;
    private $loginForm;
    private $data;

    /**
     * The constructor.
     *
     * @param Twig $twig The twig engine
     */
    public function __construct(
        LoggerFactory $loggerFactory,
        Twig $twig,
        AccountService $accountService
    ) {
        $this->logger = $loggerFactory->createInstance('backoffice.log', 'BoHomePageAction');
        $this->twig = $twig;
        $this->service = $accountService;
    }

    /**
     * Action.
     *
     * @param ServerRequest $request  The request
     * @param Response      $response The response
     *
     * @return ResponseInterface The response
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $token = $request->getAttribute('token');

        /*$data['index'] = $request->getAttribute('token_account')->index;
        $accountData = new AccountData($data);

        $this->service->selectByIndex($accountData);*/

        return $this->twig->render(
            $response,
            'backoffice/boHomePage.twig',
            [
                'html_title' => 'Admin',
                'active' => 1,
                'token' => $token,
                'meta_description' => 'La home page du back-office.',
                'account' => $request->getAttribute('token_account'),
            ]
        );
    }
}
