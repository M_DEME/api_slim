<?php

namespace BO\Action\Debug;

use Selective\Encoding\JsonEncoding;
use Selective\Config\Configuration;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Odan\Session\SessionInterface;
use Fig\Http\Message\StatusCodeInterface;

final class PhpSessionAction
{
    private $configuration;
    private $jsonEncoding;
    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(
        Configuration $configuration,
        JsonEncoding $jsonEncoding,
        SessionInterface $session
    ) {
        $this->configuration = $configuration;
        $this->jsonEncoding = $jsonEncoding;
        $this->session = $session;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $data = [];
        $data['id'] = $this->session->getId();
        $data['name'] = $this->session->getName();
        $data['options'] = $this->session->getOptions();
        $data['cookie'] = $this->session->getCookieParams();
        $data['count'] = $this->session->count();
        $data['all'] = $this->session->all();
        $response->getBody()->write(
            $this->jsonEncoding->encodeJson($data)
        );

        return $response->withHeader('Content-Type', 'application/json')
            ->withStatus(StatusCodeInterface::STATUS_OK)
        ;
    }
}
