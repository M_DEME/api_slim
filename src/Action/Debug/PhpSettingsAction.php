<?php

namespace BO\Action\Debug;

use Selective\Encoding\JsonEncoding;
use Selective\Config\Configuration;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Fig\Http\Message\StatusCodeInterface;

final class PhpSettingsAction
{
    private $configuration;
    private $jsonEncoding;

    public function __construct(
        Configuration $configuration,
        JsonEncoding $jsonEncoding
    ) {
        $this->configuration = $configuration;
        $this->jsonEncoding = $jsonEncoding;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $response->getBody()->write(
            $this->jsonEncoding->encodeJson($this->configuration->all())
        );

        return $response->withHeader('Content-Type', 'application/json')
            ->withStatus(StatusCodeInterface::STATUS_OK)
        ;
    }
}
