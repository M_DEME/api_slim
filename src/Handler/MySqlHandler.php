<?php

namespace BO\Handler;

use PDOStatement;
use PDO;
use Monolog\Logger;
use Monolog\Handler\AbstractProcessingHandler;

/**
 * This class is a handler for Monolog, which can be used
 * to write records in a MySQL table.
 *
 * Class MySQLHandler
 */
final class MySqlHandler extends AbstractProcessingHandler
{
    /**
     * @var PDO pdo object of database connection
     */
    protected $pdo;

    /**
     * @var bool defines whether the MySQL connection is been initialized
     */
    private $initialized = false;

    /**
     * @var PDOStatement statement to insert a new record
     */
    private $statement;

    /**
     * @var string the table to store the logs in
     */
    private $table = 'logs';

    /**
     * @var array default fields that are stored in db
     */
    private $defaultfields = ['id', 'channel', 'level', 'message', 'time', 'user_agent', 'ip'];

    /**
     * @var string[] additional fields to be stored in the database
     *
     * For each field $field, an additional context field with the name $field
     * is expected along the message, and further the database needs to have these fields
     * as the values are stored in the column name $field
     */
    private $additionalFields = [];

    /**
     * @var array
     */
    private $fields = [];

    /**
     * Constructor of this class, sets the PDO and calls parent constructor.
     *
     * @param PDO      $pdo              PDO Connector for the database
     * @param bool     $table            Table in the database to store the logs in
     * @param array    $additionalFields Additional Context Parameters to store in database
     * @param bool|int $level            Debug level which this handler should store
     * @param bool     $bubble
     */
    public function __construct(
        PDO $pdo = null,
        $table,
        $additionalFields = [],
        $level = Logger::DEBUG,
        $bubble = true
    ) {
        if (!is_null($pdo)) {
            $this->pdo = $pdo;
        }
        $this->table = $table;
        $this->additionalFields = $additionalFields;
        parent::__construct($level, $bubble);
    }

    /**
     * Writes the record down to the log of the implementing handler.
     *
     * @param  $record[]
     */
    protected function write(array $record): void
    {
        if (!$this->initialized) {
            $this->initialize();
        }

        // reset $fields with default values
        $this->fields = $this->defaultfields;

        //print_r($record);
        $msg = str_replace('"', '', $record['message']);
        $arr = explode('-', $msg);
        //print_r($arr);

        /*
         * merge $record['context'] and $record['extra'] as additional info of Processors
         * getting added to $record['extra']
         * @see https://github.com/Seldaek/monolog/blob/master/doc/02-handlers-formatters-processors.md
         */
        if (isset($record['extra'])) {
            $record['context'] = array_merge($record['context'], $record['extra']);
        }

        //'context' contains the array
        $contentArray = array_merge([
            'channel' => $record['channel'],
            'level' => $record['level'],
            'message' => $arr[2] ?? '',
            'time' => $record['datetime']->format('U'),
            'user_agent' => $arr[count($arr) - 1] ?? '',
            'ip' => $arr[0] ?? '',
        ], $record['context']);

        // unset array keys that are passed put not defined to be stored, to prevent sql errors
        foreach ($contentArray as $key => $context) {
            if (!in_array($key, $this->fields)) {
                unset($contentArray[$key], $this->fields[array_search($key, $this->fields)]);

                continue;
            }

            if (null === $context) {
                unset($contentArray[$key], $this->fields[array_search($key, $this->fields)]);
            }
        }

        $this->prepareStatement();

        //Fill content array with "null" values if not provided
        $contentArray = $contentArray + array_combine(
            $this->additionalFields,
            array_fill(0, count($this->additionalFields), null)
        );

        $this->statement->execute($contentArray);
    }

    /**
     * Initializes this handler by creating the table if it not exists.
     */
    private function initialize()
    {
        $this->pdo->exec(
            'CREATE TABLE IF NOT EXISTS `' . $this->table . '` '
            . '(id BIGINT(20) NOT NULL AUTO_INCREMENT PRIMARY KEY, channel VARCHAR(255),
            level INTEGER, message LONGTEXT, time INTEGER UNSIGNED, user_agent VARCHAR(255), ip VARCHAR(50),
            INDEX(channel) USING HASH, INDEX(level)
            USING HASH, INDEX(time) USING BTREE)'
        );

        //Read out actual columns
        $actualFields = [];
        $rs = $this->pdo->query('SELECT * FROM `' . $this->table . '` LIMIT 0');
        for ($i = 0; $i < $rs->columnCount(); ++$i) {
            $col = $rs->getColumnMeta($i);
            $actualFields[] = $col['name'];
        }

        //Calculate changed entries
        $removedColumns = array_diff(
            $actualFields,
            $this->additionalFields,
            $this->defaultfields
        );
        $addedColumns = array_diff($this->additionalFields, $actualFields);

        //Remove columns
        if (!empty($removedColumns)) {
            foreach ($removedColumns as $c) {
                $this->pdo->exec('ALTER TABLE `' . $this->table . '` DROP `' . $c . '`;');
            }
        }

        //Add columns
        if (!empty($addedColumns)) {
            foreach ($addedColumns as $c) {
                $this->pdo->exec('ALTER TABLE `' . $this->table . '` add `' . $c . '` TEXT NULL DEFAULT NULL;');
            }
        }

        // merge default and additional field to one array
        $this->defaultfields = array_merge($this->defaultfields, $this->additionalFields);

        $this->initialized = true;
    }

    /**
     * Prepare the sql statment depending on the fields that should be written to the database.
     */
    private function prepareStatement()
    {
        //Prepare statement
        $columns = '';
        $fields = '';
        foreach ($this->fields as $key => $f) {
            if ('id' == $f) {
                continue;
            }
            if (1 == $key) {
                $columns .= "{$f}";
                $fields .= ":{$f}";

                continue;
            }

            $columns .= ", {$f}";
            $fields .= ", :{$f}";
        }

        $this->statement = $this->pdo->prepare(
            'INSERT INTO `' . $this->table . '` (' . $columns . ') VALUES (' . $fields . ')'
        );
    }
}
