<?php

namespace BO\Handler;

use Throwable;
use Slim\Interfaces\ErrorRendererInterface;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpMethodNotAllowedException;
use Psr\Log\LoggerInterface;
use Monolog\Logger;
use BO\Utility\ExceptionDetail;
use BO\Factory\LoggerFactory;

/**
 * Html Error Renderer.
 */
class HtmlErrorRenderer implements ErrorRendererInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * The constructor.
     *
     * @param LoggerFactory $loggerFactory The logger factory
     */
    public function __construct(LoggerFactory $loggerFactory)
    {
        $this->logger = $loggerFactory
            //->addFileHandler('html_error.log', Logger::WARNING)
            ->createInstance(
                'html_error.log',
                'html_error_renderer',
                Logger::WARNING
            )
        ;
    }

    /**
     * Invoke.
     *
     * @param Throwable $exception           The exception
     * @param bool      $displayErrorDetails Show error details
     *
     * @return string The result
     */
    public function __invoke(
        Throwable $exception,
        bool $displayErrorDetails
    ): string {
        $detailedErrorMessage = ExceptionDetail::getExceptionText($exception);
        // Add error log entry
        $this->logger->error($detailedErrorMessage);
        if ($displayErrorDetails) {
            return $detailedErrorMessage;
        }
        // Detect error type
        if ($exception instanceof HttpNotFoundException) {
            $errorMessage = '404 Not Found';
        } elseif ($exception instanceof HttpMethodNotAllowedException) {
            $errorMessage = '405 Method Not Allowed';
        } else {
            $errorMessage = '500 Internal Server Error';
        }

        return $errorMessage;
    }
}
