<?php

namespace BO\Handler;

use Throwable;
use Slim\Interfaces\ErrorRendererInterface;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpMethodNotAllowedException;
use Psr\Log\LoggerInterface;
use Monolog\Logger;
use DomainException;
use BO\Utility\ExceptionDetail;
use BO\Factory\LoggerFactory;

/**
 * JSON Error Renderer.
 */
class JsonErrorRenderer implements ErrorRendererInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * The constructor.
     *
     * @param LoggerFactory $loggerFactory The logger factory
     */
    public function __construct(LoggerFactory $loggerFactory)
    {
        $this->logger = $loggerFactory
            //->addFileHandler('json_error.log', Logger::WARNING)
            ->createInstance(
                'json_error.log',
                'json_error_renderer',
                Logger::WARNING
            )
        ;
    }

    /**
     * Invoke.
     *
     * @param Throwable $exception           The exception
     * @param bool      $displayErrorDetails Show error details
     *
     * @return string The result
     */
    public function __invoke(Throwable $exception, bool $displayErrorDetails): string
    {
        $detailedErrorMessage = ExceptionDetail::getExceptionText($exception);
        // Add error log entry
        $this->logger->error($detailedErrorMessage);
        // Detect error type
        if ($exception instanceof HttpNotFoundException) {
            $errorMessage = '404 Not Found';
        } elseif ($exception instanceof HttpMethodNotAllowedException) {
            $errorMessage = '405 Method Not Allowed';
        } elseif ($exception instanceof DomainException) {
            $errorMessage = '409 Conflict';
        } else {
            $errorMessage = '500 Internal Server Error';
        }
        $result = [
            'error' => [
                'message' => $errorMessage,
            ],
        ];
        if ($displayErrorDetails) {
            $result['error']['trace'] = $detailedErrorMessage;
        }

        return (string) json_encode($result);
    }
}
