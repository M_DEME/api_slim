<?php

declare(strict_types=1);

namespace BO\Forms;

use BO\Forms\FieldTypes\AbstractFieldType;

abstract class Form
{
    /**
     * Logger.
     *
     * @var \Psr\Log\LoggerInterface
     */
    public $logger;

    /**
     * List of FieldTypes used by the Form.
     *
     * @var array of \Cms\Forms\FieldTypes\FieldType
     */
    protected $fields = [];

    /**
     * Id attribute of the Form.
     *
     * @var string
     */
    private $id;

    /**
     * @var \Psr\Http\Message\ServerRequestInterface
     */
    private $request;

    /**
     * Location of the Twig template.
     *
     * @var string
     */
    private $templatePath;

    /**
     * Where to send the form.
     *
     * @var string
     */
    private $action;

    private $validate;

    /**
     * @var type
     */
    // private $csrfField;

    /**
     * Form Constructor().
     *
     * @param
     *            $formId
     * @param
     *            $action
     * @param string $templatePath
     *                             form template path, relative to the project
     *                             root directory
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        string $formId,
        string $action,
        string $templatePath
    ) {
        $this->logger = $logger;
        $this->id = $formId;
        $this->action = $action;
        $this->validate = false;
        if (empty($templatePath)) {
            $this->templatePath = 'forms/default_form.twig';
        } else {
            $this->templatePath = $templatePath;
        }

        // $logger->debug(
        // 'Form::__constructor()-template Path:' . $this->templatePath);
    }

    /**
     * @return bool
     */
    public function isSubmitted(): bool
    {
        // $this->logger->debug(
        // 'Form::isSubmitted()-Id:' . $this->id);
        $this->validate = false;
        if (!empty($this->request) && ('POST' == $this->request->getMethod())) {
            $this->fillValues($this->request->getParsedBody());
            $this->validate = true;
        }

        return $this->validate;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        // $this->logger->debug(
        // 'Form::isValid()');
        $status = true;
        foreach ($this->fields as $field) {
            /*$this->logger->debug(
                'Form::isValid()-field:' . $field->getId() . ' - value:' .
                $field->getValue()
            );*/
            if (!$field->isValid()) {
                $this->logger->warning(
                    'Form::isValid()-Field is invalid:' . $field->getId()
                );
                $status = false;
                // Do not break because the fields of the form must be validated one by one.
                // break;
            }
        }

        return $status;
    }

    /**
     * @param \Slim\Views\Twig $view
     *                               Twig - view engine to generate view from
     *
     * @throws \Exception in case of duplicate fields throws an exception
     *
     * @return string - generated html string of form
     */
    public function getView(\Slim\Views\Twig $view): string
    {
        //$this->logger->debug('Form::getView()');
        return $view->fetch($this->templatePath, $this->getRenderObject($view));
    }

    /**
     * Prepares object for rendering, may be used for custom form rendering
     * in page template.
     */
    public function getRenderObject(\Slim\Views\Twig $view): array
    {
        // $this->logger->debug(
        // 'Form::getRenderObject()');
        $args = [];
        $args['fields'] = [];
        $args['rows'] = [];

        $args['validate'] = $this->validate;
        $args['form'] = $this;
        foreach ($this->fields as $field) {
            if (array_key_exists($field->getId(), $args['fields'])) {
                throw new \Exception(
                    'duplicate field id ' . $field->getId() . ' in form'
                );
            }
            $args['fields'][$field->getId()] = $field;
            $args['rows'][$field->getId()] = $field->getView(
                $view,
                $this->validate
            );
        }

        // $this->logger->debug('Form::getRenderObject()-Args:' . json_encode($args, true));
        return $args;
    }

    public function getId(): string
    {
        // $this->logger->debug(
        // 'Form::getId()-Id:' . $this->id);
        return $this->id;
    }

    public function getAction(): string
    {
        // $this->logger->debug(
        // 'Form::getAction()');
        return $this->action;
    }

    /**
     * @return \Psr\Http\Message\ServerRequestInterface Request
     */
    public function getRequest(): \Psr\Http\Message\ServerRequestInterface
    {
        // $this->logger->debug(
        // 'Form::getRequest()');
        return $this->request;
    }

    public function setRequest(
        \Psr\Http\Message\ServerRequestInterface $request
    ) {
        // $this->logger->debug(
        // 'Form::setRequest()');
        $this->request = $request;
    }

    /**
     * @param $field abstractFieldType
     *            Field to add to form
     */
    protected function add(AbstractFieldType $field)
    {
        // $this->logger->debug(
        // 'Form::add()-Field:' . $field->getId());
        array_push($this->fields, $field);
    }

    private function fillValues(array $allPostPutVars)
    {
        // $this->logger->debug(
        // 'Form::fillValues()-allPostPutVars:' .
        // json_encode(
        // $allPostPutVars,
        // JSON_PRETTY_PRINT));
        foreach ($this->fields as $field) {
            $id = $this->id . '_' . $field->getId();
            //$this->logger->debug('Form::fillValues()-field_id:' . $id);
            if (array_key_exists($id, $allPostPutVars)) {
                $this->logger->debug(
                    'Form::fillValues()-Field:' . $id,
                    [
                        'value' => $allPostPutVars[$id],
                    ]
                );
                $val = $allPostPutVars[$id];
                if (is_array($val)) {
                    $field->setValues($val);
                } else {
                    $field->setValue($val);
                }
            }
        }
    }
}
