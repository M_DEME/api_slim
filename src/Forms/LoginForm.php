<?php

declare(strict_types=1);

namespace BO\Forms;

use BO\Forms\FieldTypes\PasswordFieldType;
use BO\Forms\FieldTypes\HiddenFieldType;
use BO\Forms\FieldTypes\EmailFieldType;

final class LoginForm extends Form
{
    private $emailField;

    private $passwordField;

    private $tokenField;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        string $formId,
        string $action,
        string $templatePath = ''
    ) {
        parent::__construct($logger, $formId, $action, $templatePath);

        $this->emailField = new EmailFieldType($this, 'Email');
        $this->add(
            $this->emailField->setId('email')
                ->setSrOnly(true)
                ->setPlaceholder('Tapez votre email...')
                ->setRequired(true)
                ->setErrorMessage('Votre email n\'est pas valide...')
        );

        $this->passwordField = new PasswordFieldType(
            $this,
            'Password'
        );
        $this->add(
            $this->passwordField->setId('inputPassword')
                ->setSrOnly(true)
                ->setPlaceholder('Tapez votre mot de passe...')
                ->setRequired(true)
                ->setErrorMessage('Votre mot de passe n\'est pas valide...')
        );
        $this->tokenField = new HiddenFieldType($this, 'hidden');
        $this->add($this->tokenField->setId('token'));
    }

    public function getEmail(): EmailFieldType
    {
        return $this->emailField;
    }

    public function getPassword(): PasswordFieldType
    {
        return $this->passwordField;
    }

    public function getToken(): HiddenFieldType
    {
        return $this->tokenField;
    }

    public function setToken(string $token): Form
    {
        $this->tokenField->setValue($token);

        return $this;
    }
}
