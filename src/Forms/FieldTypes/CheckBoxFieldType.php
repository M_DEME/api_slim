<?php

declare(strict_types=1);

namespace BO\Forms\FieldTypes;

use BO\Forms\Form;

final class CheckBoxFieldType extends AbstractFieldType
{
    protected $values;

    /**
     * @param $owner form
     *            - slimsky form class this field will be used in
     * @param $title -
     *            Title of the field, will be rendered in template
     * @param $templatePath -
     *            template path, relative to the project root directory
     */
    public function __construct(
        Form $owner,
        string $title,
        string $templatePath = 'forms/fields/checkbox_field.twig'
    ) {
        parent::__construct($owner, $title, $templatePath);
    }

    public function isValid(): bool
    {
        return parent::isValid();
    }

    /**
     * @param \Slim\Views\Twig $view
     *                                   Twig
     * @param bool             $validate
     *
     * @return string
     */
    public function getView(\Slim\Views\Twig $view, $validate = false): string
    {
        // $this->owner->logger->debug(
        // 'NumberField::getView() - Title:' . $this->title . '-Id:' . $this->id .
        // '-Validate:' . $validate);
        $parameters = [
            'title' => $this->title,
            'id' => $this->id,
            'required' => $this->required,
            'readonly' => $this->readonly,
            'disabled' => $this->disabled,
            'sr_only' => $this->srOnly,
            'placeholder' => $this->placeholder,
            'description' => $this->description,
            'value' => $this->value,
            'values' => $this->values,
            'pattern' => $this->pattern,
            'ownerId' => $this->owner->getId(),
            'error' => $this->errorMessage,
            'isValid' => $this->isValid,
            'validate' => $validate,
        ];

        return $view->fetch($this->templatePath, $parameters);
    }

    public function getValues(): array
    {
        return $this->values;
    }

    public function setValues(array $values): self
    {
        $this->values = $values;

        return $this;
    }
}
