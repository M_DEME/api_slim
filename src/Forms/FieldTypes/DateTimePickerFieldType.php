<?php

declare(strict_types=1);

namespace BO\Forms\FieldTypes;

use Respect\Validation\Validator as v;
use BO\Forms\Form;

final class DateTimePickerFieldType extends AbstractFieldType
{
    /**
     * @param $owner form
     *            - slimsky form class this field will be used in
     * @param $title -
     *            Title of the field, will be rendered in template
     * @param $templatePath -
     *            template path, relative to the project root directory
     */
    public function __construct(
        Form $owner,
        string $title,
        string $templatePath = 'forms/fields/datetime_picker_field.twig'
    ) {
        parent::__construct($owner, $title, $templatePath);
    }

    public function isValid(): bool
    {
        $this->isValid = true;
        if (!parent::isValid()) {
            $this->isValid = false;
        } else {
            if ($this->required) {
                $this->isValid = v::date('d/m/Y H:i')->validate($this->value);
            }
        }

        return $this->isValid;
    }
}
