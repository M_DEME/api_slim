<?php

declare(strict_types=1);

namespace BO\Forms\FieldTypes;

use BO\Forms\Form;

final class SelectFieldType extends AbstractFieldType
{
    private $values;
    private $multiple;
    private $selected;
    private $multipleSelected;

    /**
     * @param $owner form
     *            - slimsky form class this field will be used in
     * @param $title -
     *            Title of the field, will be rendered in template
     * @param $templatePath -
     *            template path, relative to the project root directory
     */
    public function __construct(
        Form $owner,
        string $title,
        string $templatePath = 'forms/fields/select_field.twig'
    ) {
        parent::__construct($owner, $title, $templatePath);
    }

    public function isValid(): bool
    {
        //return parent::isValid();

        return true;
    }

    /**
     * @param \Slim\Views\Twig $view
     *                                   Twig
     * @param bool             $validate
     *
     * @return string
     */
    public function getView(\Slim\Views\Twig $view, $validate = false): string
    {
        // $this->owner->logger->debug(
        // 'NumberField::getView() - Title:' . $this->title . '-Id:' . $this->id .
        // '-Validate:' . $validate);
        $parameters = [
            'title' => $this->title,
            'id' => $this->id,
            'required' => $this->required,
            'readonly' => $this->readonly,
            'disabled' => $this->disabled,
            'sr_only' => $this->srOnly,
            'placeholder' => $this->placeholder,
            'description' => $this->description,
            'value' => $this->value,
            'values' => $this->values,
            'pattern' => $this->pattern,
            'ownerId' => $this->owner->getId(),
            'error' => $this->errorMessage,
            'isValid' => $this->isValid,
            'validate' => $validate,
            'multiple' => $this->multiple,
            'selected' => $this->selected,
            'multipleSelected' => $this->multipleSelected,
        ];

        return $view->fetch($this->templatePath, $parameters);
    }

    public function getMultiple(): bool
    {
        return $this->multiple;
    }

    public function setMultiple(bool $multiple): self
    {
        $this->multiple = $multiple;

        return $this;
    }

    public function getValues(): array
    {
        return $this->values;
    }

    public function setValues(array $values): self
    {
        $this->values = $values;

        return $this;
    }

    public function getSelected(): int
    {
        return $this->selected;
    }

    public function setSelected(int $selected): self
    {
        $this->multipleSelected = [];
        $this->selected = $selected;

        return $this;
    }

    public function getMultipleSelected(): array
    {
        return $this->multipleSelected;
    }

    public function setMultipleSelected(array $multipleSelected): self
    {
        $this->selected = 0;
        $this->multipleSelected = $multipleSelected;

        return $this;
    }
}
