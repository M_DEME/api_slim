<?php

declare(strict_types=1);

namespace BO\Forms\FieldTypes;

use Respect\Validation\Validator as v;
use BO\Forms\Form;

abstract class AbstractFieldType
{
    /**
     * Id attribute of the Field.
     *
     * @var string
     */
    protected $id;

    protected $title;

    protected $srOnly;

    protected $description;

    protected $placeholder;

    protected $value;

    protected $owner;

    protected $pattern;

    protected $disabled;

    protected $readonly;

    protected $required;

    protected $templatePath;

    protected $errorMessage;

    protected $isValid = false;

    /**
     * @param \BO\Forms\Form $owner
     *                              Form - slimsky form class this field will be used in
     * @param $title -
     *            Title of the field, will be rendered in template
     * @param $templatePath -
     *            template path, relative to the project root directory
     */
    public function __construct(Form $owner, string $title, string $templatePath)
    {
        $this->owner = $owner;
        $this->title = $title;
        $this->templatePath = $templatePath;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setSrOnly(bool $srOnly): self
    {
        $this->srOnly = $srOnly;

        return $this;
    }

    public function setPattern(string $pattern): self
    {
        $this->pattern = $pattern;

        return $this;
    }

    public function setReadOnly(bool $readonly): self
    {
        $this->readonly = $readonly;

        return $this;
    }

    public function setDisabled(bool $disabled): self
    {
        $this->disabled = $disabled;

        return $this;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function setPlaceholder(string $placeholder): self
    {
        $this->placeholder = $placeholder;

        return $this;
    }

    public function getValue(): string
    {
        return empty($this->value) ? '' : $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    public function setErrorMessage(string $value): self
    {
        $this->errorMessage = $value;

        return $this;
    }

    public function setRequired(bool $required): self
    {
        $this->required = $required;

        return $this;
    }

    public function isValid(): bool
    {
        $this->isValid = true;
        //$this->owner->logger->debug('isValid()');
        if ($this->required) {
            $this->isValid = v::notBlank()->validate($this->value);
            /*$this->owner->logger->debug(
                'AbstractField::isValid()-id:' . $this->id . '-Value:' .
                $this->value . '-Status:' . $this->isValid
            );*/
        }

        return $this->isValid;
    }

    public function setValidToFalse()
    {
        $this->isValid = false;
    }

    /**
     * @param \Slim\Views\Twig $view
     *                                   Twig
     * @param bool             $validate
     *
     * @return string
     */
    public function getView(\Slim\Views\Twig $view, $validate = false): string
    {
        // $this->owner->logger->debug(
        // 'AbstractFieldType::getView() - Title:' . $this->title . '-Id:' .
        // $this->id . '-Validate:' . $validate);
        $parameters = [
            'title' => $this->title,
            'id' => $this->id,
            'required' => $this->required,
            'readonly' => $this->readonly,
            'disabled' => $this->disabled,
            'sr_only' => $this->srOnly,
            'placeholder' => $this->placeholder,
            'description' => $this->description,
            'value' => $this->value,
            'pattern' => $this->pattern,
            'ownerId' => $this->owner->getId(),
            'error' => $this->errorMessage,
            'isValid' => $this->isValid,
            'validate' => $validate,
        ];

        // $this->owner->logger->debug(
        // json_encode(
        // $parameters,
        // JSON_PRETTY_PRINT));

        return $view->fetch($this->templatePath, $parameters);
    }
}
