<?php

declare(strict_types=1);

namespace BO\Forms\FieldTypes;

use BO\Forms\Form;

final class FileFieldType extends AbstractFieldType
{
    /**
     * @param $owner form
     *            - slimsky form class this field will be used in
     * @param $title -
     *            Title of the field, will be rendered in template
     * @param $templatePath -
     *            template path, relative to the project root directory
     */
    public function __construct(
        Form $owner,
        string $title,
        string $templatePath = 'forms/fields/file_field.twig'
    ) {
        parent::__construct($owner, $title, $templatePath);
    }

    public function isValid(): bool
    {
        return parent::isValid();
    }
}
