<?php

declare(strict_types=1);

namespace BO\Forms\FieldTypes;

use Respect\Validation\Validator as v;
use BO\Forms\Form;

final class NumberFieldType extends AbstractFieldType
{
    private $min;

    private $max;

    private $step;

    /**
     * @param $owner form
     *            - slimsky form class this field will be used in
     * @param $title -
     *            Title of the field, will be rendered in template
     * @param $templatePath -
     *            template path, relative to the project root directory
     */
    public function __construct(
        Form $owner,
        string $title,
        string $templatePath = 'forms/fields/number_field.twig'
    ) {
        parent::__construct($owner, $title, $templatePath);
    }

    public function isValid(): bool
    {
        $this->isValid = true;
        if ($this->required) {
            if (!parent::isValid()) {
                $this->isValid = false;
            } else {
                $this->isValid = v::numeric()->validate($this->value);
            }
            if (!empty($this->min)) {
                $this->isValid = v::numeric()->validate($this->min);
            }
            if (!empty($this->max)) {
                $this->isValid = v::numeric()->validate($this->max);
            }
            if (!empty($this->step)) {
                $this->isValid = v::numeric()->validate($this->step);
            }
            if (!empty($this->min) && (!empty($this->max))) {
                $this->isValid = v::between($this->min, $this->max)->validate(
                    $this->value
                );
            }
        }

        return $this->isValid;
    }

    /**
     * @param \Slim\Views\Twig $view
     *                                   Twig
     * @param bool             $validate
     *
     * @return string
     */
    public function getView(\Slim\Views\Twig $view, $validate = false): string
    {
        // $this->owner->logger->debug(
        // 'NumberField::getView() - Title:' . $this->title . '-Id:' . $this->id .
        // '-Validate:' . $validate);
        $parameters = [
            'title' => $this->title,
            'id' => $this->id,
            'required' => $this->required,
            'readonly' => $this->readonly,
            'disabled' => $this->disabled,
            'sr_only' => $this->srOnly,
            'placeholder' => $this->placeholder,
            'description' => $this->description,
            'value' => $this->value,
            'min' => $this->min,
            'max' => $this->max,
            'step' => $this->step,
            'pattern' => $this->pattern,
            'ownerId' => $this->owner->getId(),
            'error' => $this->errorMessage,
            'isValid' => $this->isValid,
            'validate' => $validate,
        ];

        // $this->owner->logger->debug(
        // json_encode(
        // $parameters,
        // JSON_PRETTY_PRINT));

        return $view->fetch($this->templatePath, $parameters);
    }

    public function getMin(): int
    {
        return $this->min;
    }

    public function getMax(): int
    {
        return $this->max;
    }

    public function getStep(): int
    {
        return $this->step;
    }

    public function setMin(int $min): self
    {
        $this->min = $min;

        return $this;
    }

    public function setMax(int $max): self
    {
        $this->max = $max;

        return $this;
    }

    public function setStep(int $step): self
    {
        $this->step = $step;

        return $this;
    }
}
