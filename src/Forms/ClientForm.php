<?php

namespace BO\Forms;

use Psr\Log\LoggerInterface;
use BO\Forms\FieldTypes\TextFieldType;
use BO\Forms\FieldTypes\EmailFieldType;

final class ClientForm extends Form
{
    private $firstNameField;
    private $lastNameField;
    private $emailField;

    public function __construct(
        LoggerInterface $logger,
        string $formId,
        string $action,
        string $templatePath = ''
    ) {
        parent::__construct($logger, $formId, $action, $templatePath);

        $this->firstNameField = new TextFieldType($this, 'Prénom');
        $this->add(
            $this->firstNameField->setId('first_name')
                ->setRequired(true)
                ->setPlaceholder('Tapez votre prénom')
                ->setSrOnly(true)
        );
        $this->lastNameField = new TextFieldType($this, 'Nom');
        $this->add(
            $this->lastNameField->setId('last_name')
                ->setRequired(true)
                ->setPlaceholder('Tapez votre nom')
                ->setSrOnly(true)
        );

        $this->emailField = new EmailFieldType($this, 'Email');
        $this->add(
            $this->emailField->setId('email')
                ->setRequired(true)
                ->setPlaceholder('Tapez votre email')
                ->setSrOnly(true)
                ->setErrorMessage('Le format de l\'email est mauvais !')
        );
    }

    public function getFirstNameField(): TextFieldType
    {
        return $this->firstNameField;
    }

    public function getLastNameField(): TextFieldType
    {
        return $this->lastNameField;
    }

    public function getEmailField(): EmailFieldType
    {
        return $this->emailField;
    }
}
