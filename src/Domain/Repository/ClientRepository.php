<?php

namespace BO\Domain\Repository;

use PDOException;
use PDO;
use BO\Factory\LoggerFactory;
use BO\Domain\Data\ClientData;

class ClientRepository
{
    private const SELECT_CLIENT_BY_INDEX_QUERY = '
        SELECT * FROM client

    ';
    private const SELECT_CLIENT_BY_LAST_NAME_QUERY = '
        SELECT * FROM client
            WHERE client_last_name = :last_name
    ';
    private const SELECT_ALL_CLIENT_QUERY = '
        SELECT * FROM client;
    ';
    private const INSERT_CLIENT_QUERY = '
        INSERT INTO client (client_last_name,client_first_name,client_address,
                            client_postal_code,client_city,client_desk_phone,
                            client_phone,client_email)
            VALUES(:lastname, :firstname, :address, :postal_code, :city, :desk_phone, :phone, :email)
    ';

    private const UPDATE_CLIENT_QUERY = '
        UPDATE client SET client_last_name = :lastname,client_first_name = :firstname,client_address = :address,
                            client_postal_code = :postal_code,client_city = :city,client_desk_phone = :desk_phone,
                            client_phone = :phone,client_email = :email
            WHERE client_index = :index
    ';

//    private const UPDATE_CLIENT_QUERY = '
//        UPDATE client SET client_last_name = :lastname
//            WHERE client_index = :index
//    ';

    private $logger;
    private $pdo;

    public function __construct(LoggerFactory $loggerFactory, PDO $pdo)
    {
        $this->pdo = $pdo;
        $this->logger = $loggerFactory->createInstance('client.log', 'ClientRepository');
    }

    public function create(ClientData $data): bool
    {
        //$this->logger->debug('create', ['player' => $data]);

        try {
            $stmt = $this->pdo->prepare(self::INSERT_CLIENT_QUERY);
            if ($stmt) {
                $stmt->bindParam(':lastname', $data->lastName, \PDO::PARAM_STR, 50);
                $stmt->bindParam(':firstname', $data->firstName, \PDO::PARAM_STR, 50);
                $stmt->bindParam(':address', $data->address, \PDO::PARAM_STR, 255);
                $stmt->bindParam(':postal_code', $data->postalCode, \PDO::PARAM_STR, 5);
                $stmt->bindParam(':city', $data->city, \PDO::PARAM_STR, 255);
                $stmt->bindParam(':desk_phone', $data->deskPhone, \PDO::PARAM_INT, 13);
                $stmt->bindParam(':phone', $data->phone, \PDO::PARAM_INT, 13);
                $stmt->bindParam(':email', $data->email, \PDO::PARAM_STR, 50);
                if ($stmt->execute()) {
                    $data->index = $this->pdo->lastInsertId();

                    return true;
                }
            }
        } catch (PDOException $e) {
            $this->logger->warning(
                'PlayerRepository::create() failed - exception:' .
                $e->getMessage(),
                ['client' => $data]
            );
        }

        return false;
    }

    public function selectAll(): array
    {
        try {
            $stmt = $this->pdo->prepare(self::SELECT_ALL_CLIENT_QUERY);
            if ($stmt) {
                if ($stmt->execute()) {
                    return $stmt->fetchAll(\PDO::FETCH_ASSOC);
                }
            }
        } catch (PDOException $e) {
            $this->logger->warning(
                'ClientRepository::selectAll() failed - exception:' .
                $e->getMessage()
            );
        }

        return [];
    }

    public function updateClient(ClientData $data): bool
    {
        try {
            $stmt = $this->pdo->prepare(self::UPDATE_CLIENT_QUERY);

            if ($stmt) {
                $stmt->bindParam(':index', $data->index, \PDO::PARAM_INT);

                $stmt->bindParam(':lastname', $data->lastName, \PDO::PARAM_STR, 50);
                $stmt->bindParam(':firstname', $data->firstName, \PDO::PARAM_STR, 50);
                $stmt->bindParam(':address', $data->address, \PDO::PARAM_STR, 255);
                $stmt->bindParam(':postal_code', $data->postalCode, \PDO::PARAM_STR, 5);
                $stmt->bindParam(':city', $data->city, \PDO::PARAM_STR, 255);
                $stmt->bindParam(':desk_phone', $data->deskPhone, \PDO::PARAM_INT, 13);
                $stmt->bindParam(':phone', $data->phone, \PDO::PARAM_INT, 13);
                $stmt->bindParam(':email', $data->email, \PDO::PARAM_STR, 50);

                return $stmt->execute();
            }
        } catch (PDOException $e) {
            $this->logger->warning(
                'PlayerRepository::create() failed - exception:' .
                $e->getMessage(),
                ['client' => $data]
            );
        }

        return false;
    }
}
