<?php


namespace BO\Domain\Repository;


use BO\Domain\Data\AccountData;
use BO\Domain\Data\TicketData;
use BO\Factory\LoggerFactory;
use PDO;
use PDOException;

class TicketRepository
{
      private const SELECT_ALL_TICKET = '
          SELECT *FROM ticket 
      ';
      private const CREATE_TICKET = '
        INSERT INTO ticket (date,texte,severite) values (:date,:texte,:severite)
      ';
    private $logger;
    private $pdo;

    public function __construct(LoggerFactory $loggerFactory, PDO $pdo)
    {
        $this->pdo = $pdo;
        $this->logger = $loggerFactory
            ->createInstance('ticket.log', 'TicketRepository')
        ;
    }

    public function readAll(): array
    {
        try {
            $stmt = $this->pdo->prepare(self::SELECT_ALL_TICKET);
            if ($stmt) {
                if ($stmt->execute()) {
                    return $stmt->fetchAll(\PDO::FETCH_ASSOC);
                }
            }
        } catch (PDOException $e) {
            $this->logger->warning(
                'TicketRepository::selectAll() failed - exception:' .
                $e->getMessage()
            );
        }
        return [];
    }

    // create ticket
    public function create(TicketData $data): bool
    {
        try {
            $stmt = $this->pdo->prepare(self::CREATE_TICKET);
            if ($stmt) {
                $stmt->bindParam(':date', $data->date, \PDO::PARAM_STR, 255);
                $stmt->bindParam(':texte', $data->texte, \PDO::PARAM_STR, 255);
                $stmt->bindParam(':severite', $data->severite, \PDO::PARAM_STR, 255);
                if ($stmt->execute()) {
                    $data->id = $this->pdo->lastInsertId();

                    return true;
                }
            }
        } catch (PDOException $e) {
            $this->logger->warning(
                'TicketRepository::create() failed - exception:' .
                $e->getMessage(),
                ['ticket' => $data]
            );
        }
        return false;
    }
}