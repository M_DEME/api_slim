<?php

namespace BO\Domain\Repository;

use PDOException;
use PDO;
use BO\Factory\LoggerFactory;
use BO\Domain\Data\AccountData;

final class AccountRepository
{
    private const SELECT_ACCOUNT_BY_EMAIL_QUERY = '
        SELECT * FROM account
            WHERE account_email = :email
    ';

    private const SELECT_ACCOUNT_BY_INDEX_QUERY = '
        SELECT * FROM account
            WHERE account_index = :index
    ';

    private const CREATE_ACCOUNT_QUERY = '
        INSERT INTO account
            (account_email, account_avatar, account_api_key, account_password)
        VALUES (:email, :avatar, :api_key, :password)
    ';
    private const DELETE_ACCOUNT_QUERY = '
        DELETE FROM account WHERE account_index = :index
    ';
    private $logger;
    private $pdo;

    public function __construct(LoggerFactory $loggerFactory, PDO $pdo)
    {
        $this->pdo = $pdo;
        $this->logger = $loggerFactory
            ->createInstance('account.log', 'AccountRepository')
        ;
    }

    public function selectByEmail(AccountData $data): bool
    {
        try {
            $stmt = $this->pdo->prepare(self::SELECT_ACCOUNT_BY_EMAIL_QUERY);
            if ($stmt) {
                $stmt->bindParam(':email', $data->email, \PDO::PARAM_STR, 50);
                if ($stmt->execute()) {
                    $result = $stmt->fetch(\PDO::FETCH_ASSOC);

                    if (empty($result)) {
                        return false;
                    }
                    $data->reload($result);

                    return true;
                }
            }
        } catch (PDOException $e) {
            $this->logger->warning(
                'AccountRepository::selectByEmail() failed - exception:' .
                $e->getMessage(),
                ['account' => $data]
            );
        }

        return false;
    }

    public function selectByIndex(AccountData $data): bool
    {
        try {
            $stmt = $this->pdo->prepare(self::SELECT_ACCOUNT_BY_INDEX_QUERY);
            if ($stmt) {
                $stmt->bindParam(':index', $data->index, \PDO::PARAM_INT);
                if ($stmt->execute()) {
                    $result = $stmt->fetch(\PDO::FETCH_ASSOC);

                    if (empty($result)) {
                        return false;
                    }
                    $data->reload($result);

                    return true;
                }
            }
        } catch (PDOException $e) {
            $this->logger->warning(
                'AccountRepository::selectByIndex() failed - exception:' .
                $e->getMessage(),
                ['account' => $data]
            );
        }

        return false;
    }

    public function create(AccountData $data): bool
    {
        try {
            $stmt = $this->pdo->prepare(self::CREATE_ACCOUNT_QUERY);
            if ($stmt) {
                $stmt->bindParam(':email', $data->email, \PDO::PARAM_STR, 255);
                $stmt->bindParam(':avatar', $data->avatar, \PDO::PARAM_STR, 255);
                $stmt->bindParam(':api_key', $data->apiKey, \PDO::PARAM_STR, 255);
                $stmt->bindParam(':password', $data->password, \PDO::PARAM_STR, 255);
                if ($stmt->execute()) {
                    $data->index = $this->pdo->lastInsertId();

                    return true;
                }
            }
        } catch (PDOException $e) {
            $this->logger->warning(
                'AccountRepository::create() failed - exception:' .
                $e->getMessage(),
                ['account' => $data]
            );
        }

        return false;
    }

    public function delete(AccountData $data): bool
    {
        try {
            $stmt = $this->pdo->prepare(self::DELETE_ACCOUNT_QUERY);
            if ($stmt) {
                $stmt->bindParam(':index', $data->index, \PDO::PARAM_INT);

                return $stmt->execute();
            }
        } catch (\PDOException $e) {
            $this->logger->warning(
                'AccountRepository::delete() failed - exception:' .
                $e->getMessage(),
                ['account' => $data]
            );
        }

        return false;
    }
}
