<?php

namespace BO\Domain\Data;

use Selective\ArrayReader\ArrayReader;

final class AccountData
{
    public $index;
    public $email;
    public $avatar;
    public $apiKey;
    public $role;
    public $password;
    public $isBlocked;
    public $isEmailvalidated;
    public $token;
    public $ip;
    public $userAgent;

    public function __construct(array $array = [])
    {
        $data = new ArrayReader($array);
        $this->index = $data->findInt('index', 0);
        $this->email = $data->findString('email', '');
        $this->avatar = $data->findString('avatar', '');
        $this->apiKey = $data->findString('apiKey', bin2hex(random_bytes(16)));
        //$this->role = $data->findString('role', '');
        $this->role = new AccountRoleData($data->findString('role', AccountRoleData::OPERATOR));
        $this->password = $data->findString('password', '');
        $this->isBlocked = $data->findBool('isBlocked', false);
        $this->isEmailvalidated = $data->findBool('isEmailValidated', false);
    }

    /**
     * Used by the repository only.
     *
     * @param array $array
     *
     * @return bool
     */
    public function reload(array $array = []): bool
    {
        $data = new ArrayReader($array);
        $this->index = $data->findInt('account_index', 0);
        $this->email = $data->findString('account_email', '');
        $this->avatar = $data->findString('account_avatar', '');
        $this->apiKey = $data->findString('account_api_key', bin2hex(random_bytes(16)));
        //$this->role = $data->findString('account_role', '');
        $this->role = new AccountRoleData($data->findString('role', AccountRoleData::OPERATOR));
        $this->password = $data->findString('account_password', '');
        $this->isBlocked = $data->findBool('account_is_blocked', false);
        $this->isEmailvalidated = $data->findBool('account_is_email_validated', false);

        return true;
    }
}
