<?php


namespace BO\Domain\Data;


use Selective\ArrayReader\ArrayReader;

class TicketData
{
    public  $id;
    public  $date;
    public  $texte;
    public  $severite;

    public function __construct(array $array = [])
    {
        $data = new ArrayReader($array);
        $this->id = $data->findInt('id', 0);
        $this->date = $data->findString('date', '');
        $this->texte = $data->findString('texte', '');
        $this->severite = $data->findString('severite', '');
    }

    /**
     * Used by the repository only.
     *
     * @param array $array
     *
     * @return bool
     */
    public function reload(array $array = []): bool
    {
        $data = new ArrayReader($array);
        $this->id = $data->findInt('ticket_id', 0);
        $this->date = $data->findString('ticket_date', '');
        $this->texte = $data->findString('ticket_texte', '');
        $this->severite = $data->findString('ticket_severite', '');
        return true;
    }
}