<?php

namespace BO\Domain\Data;

use Stringy\Stringy as S;
use Selective\ArrayReader\ArrayReader;

class ClientData
{
    public $index;
    public $lastName;
    public $firstName;
    //public $address;
    //public $postalCode;
    //public $city;
    //public $deskPhone;
    //public $phone;
    public $email;

    public function __construct(array $array = [])
    {
        $data = new ArrayReader($array);
        $this->index = $data->findInt('index', 0);
        $this->lastName = (string) S::create($data->findString('lastName', ''))->toUpperCase();
        //$this->lastName = $data->findString('lastName', '');
        //$this->firstName = $data->findString('firstName', '');
        $this->firstName = (string) S::create($data->findString('firstName', ''))->upperCaseFirst();
        /*$this->address = $data->findString('firstName', '');
        $this->postalCode = $data->findString('postalCode', '');
        $this->city = $data->findString('city', '');
        $this->deskPhone = $data->findInt('deskPhone', 0);
        $this->phone = $data->findInt('phone', 0);*/
        $this->email = $data->findString('email', '');
    }

    public function reload(array $array = []): bool
    {
        $data = new ArrayReader($array);
//        print_r($data);
        $this->index = $data->findInt('client_index', 0);
        $this->lastName = $data->findString('client_last_name', '');
        $this->firstName = $data->findString('client_first_name', '');
        $this->address = $data->findString('client_address', '');
        $this->postalCode = $data->findString('client_postal_code', '');
        $this->city = $data->findString('client_city', '');
        $this->deskPhone = $data->findInt('client_desk_phone', 0);
        $this->phone = $data->findInt('client_phone', 0);
        $this->email = $data->findString('client_email', '');

        return true;
    }
}
