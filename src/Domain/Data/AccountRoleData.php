<?php

namespace BO\Domain\Data;

final class AccountRoleData
{
    public const ADMIN = 'admin';
    public const OPERATOR = 'operator';

    private $value;

    public function __construct(string $role)
    {
        switch ($role) {
            case 'admin':
                $this->value = self::ADMIN;

                break;
            case 'operator':
            default:
                $this->value = self::OPERATOR;

                break;
        }
    }
}
