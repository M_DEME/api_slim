<?php


namespace BO\Domain\Validator;


use BO\Domain\Data\ClientData;
use Selective\Validation\ValidationResult;

class ClientValidator
{
    public $validation;

    public function __construct()
    {
        $this->validation = new ValidationResult();
    }

    public function validateName(ClientData $data): bool
    {
        if (empty($data->lastName)) {
            $this->validation->addError('lastName', 'Input required');

            return false;
        }

        return true;
    }
}