<?php

namespace BO\Domain\Validator;

use Selective\Validation\ValidationResult;
use BO\Domain\Data\AccountData;

final class AccountValidator
{
    public $validation;

    public function __construct()
    {
        $this->validation = new ValidationResult();
    }

    public function validateEmail(AccountData $data)
    {
        if (empty($data->email)) {
            $this->validation->addError('email', 'Input required');
        } elseif (false === filter_var($data->email, FILTER_VALIDATE_EMAIL)) {
            $this->validation->addError('email', 'Invalid email format');
        }
    }
}
