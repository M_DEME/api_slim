<?php

namespace BO\Domain\Service;

use Selective\Validation\Exception\ValidationException;
use BO\Factory\LoggerFactory;
use BO\Domain\Validator\ClientValidator;
use BO\Domain\Repository\ClientRepository;
use BO\Domain\Data\ClientData;

class ClientService
{
    private $logger;
    private $repository;
    private $validator;

    public function __construct(
        LoggerFactory $logger,
        ClientRepository $clientRepository,
        ClientValidator $clientValidator
    ) {
        $this->logger = $logger->createInstance('client.log', 'ClientService');
        $this->validator = $clientValidator;
        $this->repository = $clientRepository;
    }

    public function create(ClientData $data): bool
    {
        /*$this->validator->validateName($data);

        if ($this->validator->validation->isFailed()) {
            $this->validator->validation->setMessage('client cannot be created !');

            throw new ValidationException($this->validator->validation);
        }*/

        return $this->repository->create($data);
    }

    public function selectAll(): array
    {
        return $this->repository->selectAll();
    }

    public function updateClient(ClientData $data): bool
    {
        return $this->repository->updateClient($data);
    }
}
