<?php


namespace BO\Domain\Service;


use BO\Domain\Data\ClientData;
use BO\Domain\Data\TicketData;
use BO\Domain\Repository\AccountRepository;
use BO\Domain\Repository\TicketRepository;
use BO\Domain\Validator\AccountValidator;
use BO\Factory\LoggerFactory;
use BO\Helper\AuthenticationHelper;
use Odan\Session\SessionInterface;

class TicketService
{
    private $logger;
    private $repository;
    public function __construct(
        LoggerFactory $logger,
        TicketRepository $ticketRepository
    ) {
        $this->logger = $logger->createInstance('ticket.log', 'TicketService');
        $this->repository = $ticketRepository;
    }

    public function readAll(): array
    {
        return $this->repository->readAll();
    }
    public function create(TicketData $data): bool
    {
        return $this->repository->create($data);
    }
}