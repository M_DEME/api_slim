<?php

namespace BO\Domain\Service;

use Selective\Validation\Exception\ValidationException;
use Odan\Session\SessionInterface;
use BO\Helper\AuthenticationHelper;
use BO\Factory\LoggerFactory;
use BO\Domain\Validator\AccountValidator;
use BO\Domain\Repository\AccountRepository;
use BO\Domain\Data\AccountRoleData;
use BO\Domain\Data\AccountData;

final class AccountService
{
    private const GUEST = 'guest4vitobois';
    private const GUEST_API = 'loginApiGuest4VitoBois';

    public $authentication;
    private $logger;
    private $repository;
    private $validator;
    private $session;

    public function __construct(
        LoggerFactory $logger,
        AccountRepository $accountRepository,
        AccountValidator $accountValidator,
        AuthenticationHelper $authenticationHelper,
        SessionInterface $session
    ) {
        $this->logger = $logger->createInstance('account.log', 'AccountService');
        $this->repository = $accountRepository;
        $this->validator = $accountValidator;
        $this->authentication = $authenticationHelper;
        $this->session = $session;
    }

    public function selectByEmail(AccountData $data): bool
    {
        return $this->repository->selectByEmail($data);
    }

    public function selectByIndex(AccountData $data): bool
    {
        return $this->repository->selectByindex($data);
    }

    public function delete(AccountData $data): bool
    {
        if (AccountRoleData::ADMIN == $data->role) {
            return $this->repository->delete($data);
        }
    }

    public function logout(): void
    {
        // Clears all session data and regenerates session ID
        $this->session->remove('account');

        /*if ($this->session->isStarted()) {
            $this->session->destroy();
        }*/
    }

    public function getLoginToken(string $ip, string $userAgent): string
    {
        $account = new AccountData([
            'email' => self::GUEST,
            'apiKey' => self::GUEST_API,
        ]);
        $account->ip = $ip;
        $account->userAgent = $userAgent;

        return $this->authentication->generateToken($account, 'now +10 minutes');
    }

    public function loginForm(AccountData $data, string $password, string $token): string
    {
        $this->validator->validateEmail($data);

        if ($this->validator->validation->isFailed()) {
            $this->validator->validation->setMessage('The account cannot be created !');

            throw new ValidationException($this->validator->validation);
        }

        $jwt = $this->authentication->decodeToken($token);

        if (!empty($jwt) && $this->repository->selectByEmail($data)) {
            // Test the password
            //$hash = hash_hmac('sha256', hex2bin($data->password), self::GUEST_API);
            $hmac = hash_hmac(
                'sha256',
                base64_encode(hex2bin($data->password)),
                $token,
                true
            );
            $hmacInBase64 = base64_encode($hmac);

            if ((0 == strcasecmp($hmacInBase64, $password))) {
                $data->token = $this->authentication->generateToken($data);
                $this->startAccountSession($data);
                $this->logger->debug(
                    'loginForm() - success',
                    [
                        //'account' => $data,
                        //'password' => $password,
                        //'hmacInBase64' => $hmacInBase64,
                        'all' => $this->session->all(),
                    ]
                );

                return $data->token;
            }
        }

        return '';
    }

    /**
     * Init user session.
     */
    private function startAccountSession(AccountData $data): void
    {
        // Clear session data
        // $this->session->destroy();
        //$this->session->start();

        // Create new session id
        $this->session->regenerateId();

        $this->session->set('account', $data);
        /*$this->logger->debug(
            'session is started:' . $this->session->isStarted(),
            ['all' => $this->session->all()]
        );*/

        $this->authentication->setAccount($data);
    }
}
