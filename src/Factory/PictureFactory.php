<?php

namespace BO\Factory;

final class PictureFactory
{
    public $uploadUrl;
    public $uploadDir;

    public function __construct(array $settings)
    {
        $this->uploadDir = $settings['path'];
        $this->uploadUrl = $settings['upload_url'];
        if (!file_exists($this->uploadDir)) {
            mkdir($this->uploadDir, 0777, true);
        }
    }

    public function saveBase64ToFile($bin64, $filename): string
    {
        $status = '';
        $type = [];
        if (preg_match('/^data:image\/(\w+);base64,/', $bin64, $type)) {
            $bin64 = substr($bin64, strpos($bin64, ',') + 1);
            $type = strtolower($type[1]); // jpg, png, gif, png

            if (in_array($type, ['jpg', 'jpeg', 'gif', 'png'])) {
                $bin64 = base64_decode($bin64);
                if (false !== $bin64) {
                    if (false !== file_put_contents($this->uploadDir . "/{$filename}", $bin64)) {
                        $status = $this->uploadUrl . "{$filename}";
                    }
                }
            }
        }

        return $status;
    }
}
