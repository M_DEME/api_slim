<?php

namespace BO\Middleware;

use Slim\Routing\RouteContext;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Odan\Session\SessionInterface;
use Fig\Http\Message\StatusCodeInterface;
use BO\Helper\AuthenticationHelper;
use BO\Factory\LoggerFactory;
use BO\Domain\Data\AccountData;

/**
 * Middleware.
 */
final class AuthSessionMiddleware implements MiddlewareInterface
{
    private $logger;
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var AuthenticationHelper
     */
    private $auth;

    /**
     * @var ResponseFactoryInterface
     */
    private $responseFactory;

    /**
     * The constructor.
     */
    public function __construct(
        LoggerFactory $loggerFactory,
        SessionInterface $session,
        AuthenticationHelper $auth,
        ResponseFactoryInterface $responseFactory
    ) {
        $this->logger = $loggerFactory->createInstance('auth.log', 'AuthSessionMiddleware');
        $this->session = $session;
        $this->auth = $auth;
        $this->responseFactory = $responseFactory;
    }

    /**
     * Invoke middleware.
     *
     * @param ServerRequestInterface  $request The request
     * @param RequestHandlerInterface $handler The handler
     *
     * @return ResponseInterface The response
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $account = $this->session->get('account');
        //$this->logger->debug('AuthSessionMiddleware:process()', ['account' => $account]);
        //var_dump($account);

        if ($account instanceof AccountData) {
            // account is logged in
            $this->auth->setAccount($account);

            $request = $request->withAttribute('token', $account->token);
            $request = $request->withAttribute('token_email', $account->email);
            $request = $request->withAttribute('token_account', $account);

            //$this->logger->debug('Session', ['account' => $account]);

            return $handler->handle($request);
        }

        $this->logger->warning(
            'process() - The account is not found in the session -> should return 401!'
        );
        // User is not logged in
        // Redirect to login page
        $routeParser = RouteContext::fromRequest($request)->getRouteParser();
        $url = $routeParser->fullUrlFor($request->getUri(), 'login');

        return $this->responseFactory->createResponse()->withHeader('Location', $url)
            ->withStatus(StatusCodeInterface::STATUS_FOUND)
        ;
    }
}
