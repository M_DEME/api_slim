<?php

namespace BO\Middleware;

use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Exception;
use BO\Helper\AuthenticationHelper;
use BO\Factory\LoggerFactory;
use BO\Domain\Repository\AccountRepository;
use BO\Domain\Data\AccountData;

class JwtMiddleware implements MiddlewareInterface
{
    private $logger;
    private $auth;
    private $accountRepository;
    private $tokenHeader;

    public function __construct(
        LoggerFactory $loggerFactory,
        AuthenticationHelper $authenticationHelper,
        AccountRepository $accountRepository
    ) {
        $this->logger = $loggerFactory->createInstance('jwtmiddleware.log', 'JwtMiddleware');
        $this->auth = $authenticationHelper;
        $this->accountRepository = $accountRepository;
        $this->tokenHeader = $this->auth->getHeader();
    }

    /**
     * Process a server request and return a response.
     */
    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        $userAgent = $request->getHeaderLine('User-Agent');
        $ip = $request->getAttribute('client-ip');
        /*$this->logger->debug(
            'JwtMiddleware::process()',
            [
                'IP' => $ip,
                'UA' => $userAgent,
            ]
        );*/

        // 1) Check the Authorization: Bearer <token> header.
        $authorization = explode(' ', (string) $request->getHeaderLine('Authorization'));
        $token = $authorization[1] ?? '';
        if (empty($token)) {
            // 2) // Look for the token (header / getter parameters or hidden input
            if (!$request->hasHeader($this->tokenHeader)) {
                if (array_key_exists($this->tokenHeader, $request->getQueryParams())) {
                    $token = $request->getQueryParams()[$this->tokenHeader];
                }
            } else {
                $token = $request->getHeaderLine($this->tokenHeader);
            }
            if (empty($token)) {
                $body = $request->getParsedBody() ?? [];
                if (array_key_exists($this->tokenHeader, $body)) {
                    $token = $body[${$this}->tokenHeader];
                }
            }
            if (empty($token)) {
                // 3) Get referer and check getter parameters
                parse_str(parse_url($request->getHeaderLine('referer'), PHP_URL_QUERY), $queries);
                if (!empty($queries)) {
                    //$this->logger->debug('process() - $queries', ['queries' => $queries]);
                    $token = $queries['X-Token'] ?? '';
                }
            }
        }
        /*$this->logger->debug(
            'JwtMiddleware::process() - Token:' . $token,
            [
                'tokenName' => $this->tokenHeader,
                'header' => $request->getHeaders(),
            ]
        );*/

        $jwt = null;
        if (empty($token) || null == $token || $token == new \stdClass()) {
            $this->logger->warning(
                'JwtMiddleware::process() - Token is empty -> should return 401!'
            );
        } else {
            try {
                $jwt = $this->auth->decodeToken($token);
            } catch (Exception $e) {
                $this->logger->warning(
                    'JwtMiddleware::process() - JWT Decoded Token exception:' . $e->getMessage()
                );
                $jwt = [];
            }
        }

        if (empty((array) $jwt)) {
            $this->logger->warning(
                'JwtMiddleware::process() - JWT Decoded Token is empty -> should return 401!'
            );
        } else {
            if (
                (0 ==
                strcmp($userAgent, $jwt->sub->ua)) &&
                (0 == strcmp($ip, $jwt->sub->ip))
            ) {
                // Check if the token account exists in the database
                // the id is correct
                // and the account is not blocked !
                $account = new AccountData(['email' => $jwt->sub->email, 'username' => $jwt->sub->email]);
                if ($this->accountRepository->selectByEmail($account)) {
                    $id = hash_hmac('sha256', "{$account->index}", $account->apiKey);
                    if (!$account->isBlocked && $id == $jwt->sub->id) {
                        $request = $request->withAttribute('token', $token);
                        $request = $request->withAttribute('token_email', $jwt->sub->email);
                        $request = $request->withAttribute('token_account', $account);
                    } else {
                        $this->logger->warning(
                            'JwtMiddleware::process() - The account is blocked or id is not the same  -> should return 401!'
                        );
                    }
                } else {
                    $this->logger->warning(
                        'JwtMiddleware::process() - The account from token is not found  -> should return 401!'
                    );
                }
            } else {
                $this->logger->warning(
                    'JwtMiddleware::process() - Token has nor ip or ua equal to the request  -> should return 401!'
                );
            }
        }

        return $handler->handle($request);
    }
}
