<?php

namespace BO\Middleware;

use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Odan\Session\SessionInterface;
use BO\Factory\LoggerFactory;

/**
 * PSR-15 Session Middleware.
 */
final class SessionMiddleware implements MiddlewareInterface
{
    private $logger;
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * Constructor.
     *
     * @param SessionInterface $session The session handler
     */
    public function __construct(
        LoggerFactory $loggerFactory,
        SessionInterface $session
    ) {
        //$this->logger = $loggerFactory->createInstance('session.log', 'SessionMiddleware');
        $this->session = $session;
    }

    /**
     * Invoke middleware.
     *
     * @param ServerRequestInterface  $request The request
     * @param RequestHandlerInterface $handler The handler
     *
     * @return ResponseInterface The response
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if (!$this->session->isStarted()) {
            $this->session->start();
        }

        return $handler->handle($request);
        $this->session->save();
        /*$this->logger->debug('After save', [
            'result' => $result, 'all' => $this->session->all(),
        ]);*/
    }
}
