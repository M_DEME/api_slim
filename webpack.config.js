const webpack = require('webpack');
const path = require('path');
const glob = require('glob');

// A webpack plugin to remove/clean your build folder(s).
const {
    CleanWebpackPlugin
} = require('clean-webpack-plugin');
// Webpack plugin for generating an asset manifest.
const ManifestPlugin = require('webpack-manifest-plugin');
// Copies individual files or entire directories, which already exist, to the build directory.
const CopyWebpackPlugin = require('copy-webpack-plugin');
// This plugin extracts CSS into separate files. It creates a CSS file per JS file
// which contains CSS. It supports On-Demand-Loading of CSS and SourceMaps.
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// A Webpack plugin to optimize \ minimize CSS assets.
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
// Remove unused CSS
const PurgeCssPlugin = require('purgecss-webpack-plugin');
// This plugin uses terser to minify your JavaScript.
const TerserJSPlugin = require('terser-webpack-plugin');

const env = process.env.NODE_ENV;
const PATHS = {
    src: path.join(__dirname, 'public/assets')
};
//console.log(PATHS);

module.exports = {
    mode: env == 'production' || env == 'none' ? env : 'development',

    entry: {
        'layout/layout': './templates/layout/layout',
        'home/home': './templates/home/home',
        'backoffice/client/client': './templates/backoffice/client/client',
    },

    output: {
        path: path.resolve(__dirname, 'public/assets'),
    },
    optimization: {
        minimize: true,
        minimizer: [new TerserJSPlugin()],
    },
    module: {
        rules: [{
            test: /\.scss$/,
            use: [
                MiniCssExtractPlugin.loader,
                'css-loader',
                {
                    loader: 'postcss-loader',
                    options: {
                        plugins: function () {
                            return [require('precss'), require('autoprefixer')];
                        }
                    }
                },
                'sass-loader'
            ]
        },
        {
            test: /\.css$/,
            use: ['style-loader', 'css-loader']
        },
        {
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader'
        },
        {
            test: /\.(png|jp(e*)g|svg|gif)$/,
            use: [{
                loader: 'url-loader',
                options: {
                    limit: 8000,
                    name: 'assets/img/[name]-[hash].[ext]'
                }
            }]
        }
        ],
    },
    plugins: [
        new CleanWebpackPlugin(),
        new ManifestPlugin(),
        new CopyWebpackPlugin([{
            from: 'resources/assets/images',
            to: 'img'
        }]),
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css'
        }),
    ],

    watchOptions: {
        ignored: ['./node_modules/']
    },
};

if (env === 'production') {
    module.exports.plugins.push(
        new OptimizeCssAssetsPlugin({
            cssProcessorPluginOptions: {
                preset: ['default', {
                    discardComments: {
                        removeAll: true
                    }
                }]
            }
        })
    );

    module.exports.plugins.push(
        new PurgeCssPlugin({
            paths: glob.sync(`${PATHS.src}/**/*`, {
                nodir: true
            }),
        })
    );
}
