<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

$settings['env'] = 'development';

$settings['app_url'] = 'http://192.168.0.19:82/';
$settings['upload_url'] = 'http://192.168.0.19:83/';

$settings['error_handler_middleware']['display_error_details'] = true;
$settings['error_handler_middleware']['log_errors'] = true;

$settings['db'] = [
    'driver' => 'mysql',
    'host' => 'localhost:3306',
    'username' => 'vitoboisxuroot',
    'password' => '4Lt5z4QbeyC2LC39',
    'database' => 'vitoboisxuroot',
    'charset' => 'utf8mb4',
    'collation' => 'utf8mb4_unicode_ci',
    'flags' => [
        PDO::ATTR_PERSISTENT => false,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_EMULATE_PREPARES => true,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8mb4 COLLATE utf8mb4_unicode_ci',
    ],
];

$settings['logger']['level'] = \Monolog\Logger::DEBUG;
$settings['locale']['cache'] = null;

$settings['twig']['debug'] = true;
$settings['twig']['cache_enabled'] = false;

$settings['picture']['app_url'] = $settings['app_url'];
$settings['picture']['upload_url'] = $settings['upload_url'];
