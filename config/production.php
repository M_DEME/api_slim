<?php

// Error reporting
error_reporting(0);
ini_set('display_errors', '0');

// Production environment
$settings['env'] = 'production';

$settings['router']['cache_file'] = $settings['cache'] . '/route-cache.php';

$settings['db'] = [
    'driver' => 'mysql',
    'host' => 'vitoboisxuroot.mysql.db',
    'username' => 'vitoboisxuroot',
    'password' => '4Lt5z4QbeyC2LC39',
    'database' => 'vitoboisxuroot',
    'charset' => 'utf8mb4',
    'collation' => 'utf8mb4_unicode_ci',
    'flags' => [
        PDO::ATTR_PERSISTENT => false,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_EMULATE_PREPARES => true,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8mb4 COLLATE utf8mb4_unicode_ci',
    ],
];
