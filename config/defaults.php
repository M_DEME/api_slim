<?php

// Timezone
date_default_timezone_set('Europe/Paris');

// Settings
$settings = [];
// Path settings
$settings['root'] = dirname(__DIR__);
$settings['cache'] = $settings['root'] . '/tmp';
$settings['log'] = $settings['root'] . '/log';
$settings['public'] = $settings['root'] . '/public';
$settings['app_url'] = 'https://vitobois.com/';
$settings['upload_url'] = 'https://upload.vitobois.com/';

// Project name
$settings['name'] = 'vitobois';

// Error Handling Middleware settings
$settings['error_handler_middleware'] = [
    // Should be set to false in production
    'display_error_details' => false,
    // Parameter is passed to the default ErrorHandler
    // View in rendered output by enabling the "displayErrorDetails" setting.
    // For the console and unit tests it should be disable too
    'log_errors' => true,
    // Display error details in error log
    'log_error_details' => true,
];

// router
$settings['router'] = [
    // Should be set only in production
    'cache_file' => '',
];

// Application settings
$settings['jwt'] = [
    'token_header' => 'X-Token',
    'secret' => '{{vitobois_secret}}',
    'app_url' => $settings['app_url'],
];

// Picture
$settings['picture'] = [
    'path' => $settings['root'] . '/upload',
    'app_url' => $settings['app_url'],
    'upload_url' => $settings['upload_url'],
];

// Logger settings
$settings['logger'] = [
    'name' => $settings['name'],
    'path' => $settings['log'],
    'filename' => $settings['name'] . '.log',
    'level' => \Monolog\Logger::WARNING,
    'file_permission' => 0775,
];

// View settings
$settings['twig'] = [
    'path' => $settings['root'] . '/templates',
    'debug' => false,
    'cache_enabled' => true,
    'cache_path' => $settings['cache'] . '/twig-cache',
];

// Session
$settings['session'] = [
    'name' => $settings['name'],
    // turn off automatic sending of cache headers entirely
    'cache_limiter' => '',
    // garbage collection
    //'gc_probability' => 1,
    //'gc_divisor' => 1,
    'gc_maxlifetime' => 30 * 24 * 60 * 60,
    // security on
    'cookie_httponly' => true,
    //'cookie_secure' => true,
];

// E-Mail settings
$settings['smtp'] = [
    'type' => 'smtp',
    'host' => '127.0.0.1',
    'port' => '25',
    'secure' => '',
    'from' => 'from@example.com',
    'from_name' => 'My name',
    'to' => 'to@example.com',
];

// Console commands
/*$settings['commands'] = [
    \App\Console\TwigCompilerCommand::class,
    \App\Console\SchemaSqlCommand::class,
];*/

return $settings;
