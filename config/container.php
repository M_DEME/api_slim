<?php

use Twig\Extension\DebugExtension;
use Slim\Views\Twig;
use Slim\Interfaces\RouteParserInterface;
use Slim\Factory\AppFactory;
use Slim\App;
use Selective\Validation\Middleware\ValidationExceptionMiddleware;
use Selective\Validation\Encoder\JsonEncoder;
use Selective\Config\Configuration;
use Selective\BasePath\BasePathMiddleware;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Container\ContainerInterface;
use Odan\Session\SessionInterface;
use Odan\Session\PhpSession;
use Odan\Session\MemorySession;
use Fullpipe\TwigWebpackExtension\WebpackExtension;
use BO\Helper\AuthenticationHelper;
use BO\Factory\PictureFactory;
use BO\Factory\LoggerFactory;

return [
    // Application settings
    Configuration::class => function () {
        return new Configuration(require __DIR__ . '/settings.php');
    },
    App::class => function (ContainerInterface $container) {
        AppFactory::setContainer($container);
        $app = AppFactory::create();

        $config = $container->get(Configuration::class);
        // settings available on production.php file
        $routeCacheFile = $config->findString('router.cache_file');
        if ($routeCacheFile) {
            $app->getRouteCollector()->setCacheFile($routeCacheFile);
        }

        return $app;
    },
    // For the responder
    ResponseFactoryInterface::class => function (ContainerInterface $container) {
        $app = $container->get(App::class);

        return $app->getResponseFactory();
    },
    BasePathMiddleware::class => function (ContainerInterface $container) {
        $app = $container->get(App::class);

        return new BasePathMiddleware($app);
    },
    // The Slim RouterParser
    RouteParserInterface::class => function (ContainerInterface $container) {
        return $container->get(App::class)->getRouteCollector()->getRouteParser();
    },
    // The logger factory
    LoggerFactory::class => function (ContainerInterface $container) {
        return new LoggerFactory($container->get(Configuration::class)->getArray('logger'));
    },
    // The picture factory
    PictureFactory::class => function (ContainerInterface $container) {
        return new PictureFactory($container->get(Configuration::class)->getArray('picture'));
    },
    // Authentication Helper
    AuthenticationHelper::class => function (ContainerInterface $container) {
        return new AuthenticationHelper(
            $container->get(Configuration::class)->getArray('jwt')
        );
    },
    ValidationExceptionMiddleware::class => static function (ContainerInterface $container) {
        $factory = $container->get(ResponseFactoryInterface::class);

        return new ValidationExceptionMiddleware($factory, new JsonEncoder());
    },
    PDO::class => function (ContainerInterface $container) {
        $config = $container->get(Configuration::class);

        $host = $config->getString('db.host');
        $dbname = $config->getString('db.database');
        $username = $config->getString('db.username');
        $password = $config->getString('db.password');
        $charset = $config->getString('db.charset');
        $flags = $config->getArray('db.flags');
        $dsn = "mysql:host={$host};dbname={$dbname};charset={$charset}";

        return new PDO($dsn, $username, $password, $flags);
    },
    // Twig templates
    Twig::class => function (ContainerInterface $container) {
        $config = $container->get(Configuration::class);
        $twigSettings = $config->getArray('twig');

        $twig = Twig::create($twigSettings['path'], [
            'cache' => $twigSettings['cache_enabled'] ? $twigSettings['cache_path'] : false,
            'debug' => $twigSettings['debug'],
        ]);

        // Add extensions
        //$twig->addExtension(new TwigTranslationExtension());
        $twig->addExtension(new WebpackExtension(
            $config->getString('public') . '/assets/manifest.json',
            '/assets/',
            '/assets/'
        ));

        if ($twigSettings['debug']) {
            $twig->addExtension(new DebugExtension());
        }

        // Add the Twig extension only we run the app from the command line / cron job,
        // but not when phpunit tests are running.
        /*if ((PHP_SAPI === 'cli' || PHP_SAPI === 'cgi-fcgi') && !defined('PHPUNIT_TEST_SUITE')) {
            $app = $container->get(App::class);
            $routeParser = $app->getRouteCollector()->getRouteParser();
            $uri = (new UriFactory())->createUri('http://localhost');

            $runtimeLoader = new TwigRuntimeLoader($routeParser, $uri);
            $twig->addRuntimeLoader($runtimeLoader);
            $twig->addExtension(new TwigExtension());
        }*/

        return $twig;
    },
    SessionInterface::class => function (ContainerInterface $container) {
        $settings = $container->get(Configuration::class)->getArray('session');

        if (PHP_SAPI === 'cli') {
            $session = new MemorySession();
        } else {
            $session = new PhpSession();
        }
        $session->setOptions($settings);

        return $session;
    },
];
