<?php

// Define app routes

use Slim\Interfaces\RouteCollectorProxyInterface;
use Slim\App;
use Monolog\Logger;
use Middlewares\AccessLog;
use BO\Middleware\JwtMiddleware;
use BO\Middleware\AuthSessionMiddleware;
use BO\Handler\MySqlHandler;
use BO\Action\PostLoginAction;
use BO\Action\LoginAction;
use BO\Action\HomeAction;
use BO\Action\Debug\PhpSettingsAction;
use BO\Action\Debug\PhpSessionAction;
use BO\Action\Debug\PhpInfoAction;
use BO\Action\Backoffice\Client\ReadClientAction;
use BO\Action\Backoffice\Client\PostClientAction;
use BO\Action\Backoffice\Client\ListDatatableClientAction;
use BO\Action\Backoffice\BoHomePageAction;
use BO\Action\Backoffice\BoClientPageAction;

//  use de l'api
use BO\Action\ApiTicket\ApiAction;
use BO\Action\ApiTicket\ListAction;
use BO\Action\ApiTicket\CreationTicketAction;

return function (App $app) {
    $container = $app->getContainer();

    $now = new DateTime('now');
    //Create the logger
    $logger = new Logger('http');
    $handler = new MySqlHandler(
        $container->get(PDO::class),
        'http-log-' . $now->format('Ymd'),
        [],
        Logger::DEBUG
    );
    $logger->pushHandler($handler);

    $accessLog = new AccessLog($logger);
    $accessLog->format(AccessLog::FORMAT_COMBINED);

    $app->get('/', HomeAction::class)->setName('root')->addMiddleware($accessLog);
    $app->get('/login', LoginAction::class)->setName('login');
    $app->post('/login', PostLoginAction::class)->setName('postLogin');

    // BACK OFFICE
    $app->group('/back/', function (RouteCollectorProxyInterface $group) {
        $group->get('home', BoHomePageAction::class)->setName('boHome');
        // CLIENT
        $group->get('client', BoClientPageAction::class)->setName('boClient');
        $group->post('client', PostClientAction::class)->setName('postClient');

        //$group->get('client', ReadClientAction::class)->setName('boReadeClient');
    })->add(AuthSessionMiddleware::class);

    // BACK-OFFICE API
    $app->group('/back/api/', function (RouteCollectorProxyInterface $group) {
        $group->get('client', ListDatatableClientAction::class);
    })->add(JwtMiddleware::class);

    // DEBUG
    $app->group('/debug/php/', function (RouteCollectorProxyInterface $group) {
        $group->get(
            'info',
            PhpInfoAction::class
        );
        $group->get(
            'settings',
            PhpSettingsAction::class
        );
        $group->get(
            'session',
            PhpSessionAction::class
        );
    });

    // API TICKET
    $app->group('/api/', function (RouteCollectorProxyInterface $group) {
        $group->get('read', ApiAction::class)->setName('read');
        $group->get('list', ListAction::class)->setName('list');
        $group->post('create', CreationTicketAction::class)->setName('create');
    });
};
