<?php

use Slim\Views\TwigMiddleware;
use Slim\Views\Twig;
use Slim\App;
use Selective\Validation\Middleware\ValidationExceptionMiddleware;
use Selective\Config\Configuration;
use Selective\BasePath\BasePathMiddleware;
use Middlewares\ResponseTime;
use Middlewares\ErrorHandler;
use Middlewares\ErrorFormatter\JsonFormatter;
use Middlewares\ErrorFormatter\HtmlFormatter;
use Middlewares\ContentType;
use Middlewares\ContentLanguage;
use Middlewares\ContentEncoding;
use Middlewares\ClientIp;
use BO\Middleware\SessionMiddleware;
use BO\Handler\JsonErrorRenderer;
use BO\Handler\HtmlErrorRenderer;

return function (App $app) {
    $container = $app->getContainer();
    /*
     * The routing middleware should be added earlier than the ErrorMiddleware
     * Otherwise exceptions thrown from it will not be handled by the middleware
     */
    $app->addRoutingMiddleware();
    // Parse json, form data and xml
    $app->addBodyParsingMiddleware();

    $app->add(ValidationExceptionMiddleware::class);
    // Add Twig-View Middleware
    $app->add(TwigMiddleware::createFromContainer($app, Twig::class));
    $app->add(BasePathMiddleware::class);

    $app->addMiddleware(new ContentType());
    $app->addMiddleware(new ContentLanguage(['en', 'fr']));
    $app->addMiddleware(new ContentEncoding());
    $app->addMiddleware(new ClientIp());
    $app->addMiddleware(new ResponseTime());
    $app->add(SessionMiddleware::class);
    // Error handler
    $settings = $container->get(Configuration::class)->getArray('error_handler_middleware');
    $displayErrorDetails = (bool) $settings['display_error_details'];
    $logErrors = (bool) $settings['log_errors'];
    $logErrorDetails = (bool) $settings['log_error_details'];

    // Create a new ErrorHandler instance
    // Any number of formatters can be added. One will be picked based on the Accept
    // header of the request. If no formatter matches, the PlainFormatter will be used.
    $errorHandler = new ErrorHandler([
        new HtmlFormatter(),
        new JsonFormatter(),
    ]);

    /**
     * @param bool $displayErrorDetails -> Should be set to false in production
     * @param bool $logErrors           -> Parameter is passed to the default ErrorHandler
     * @param bool $logErrorDetails     -> Display error details in error log
     *                                  which can be replaced by a callable of your choice.
     *
     * Note: This middleware should be added last. It will not handle any exceptions/errors
     * for middleware added after it.
     */
    $errorMiddleware = $app->addErrorMiddleware($displayErrorDetails, $logErrors, $logErrorDetails);
    //$errorMiddleware->setDefaultErrorHandler($errorHandler);

    $errorHandler = $errorMiddleware->getDefaultErrorHandler();
    $errorHandler->registerErrorRenderer('text/html', HtmlErrorRenderer::class);
    $errorHandler->registerErrorRenderer('application/json', JsonErrorRenderer::class);
};
