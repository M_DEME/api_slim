# vitobois-back

## SSH access
ssh://vitoboisxu@ssh.cluster026.hosting.ovh.net/

## MySQL v5.6
vitoboisxuroot.mysql.db
vitoboisxuroot

## no-reply Email
no-reply@vitobois.com

## Library must be used by the project:
### Stringy - A string manipulation library with multibyte support.
https://github.com/voku/Stringy
"voku/Stringy" used the functions from the "Portable UTF-8"-Class.

### Respect Validation - The most awesome validation engine ever created for PHP
https://github.com/Respect/Validation
https://respect-validation.readthedocs.io/en/1.1/

### JWT - A simple library to work with JSON Web Token and JSON Web Signature
[firebase/php-jwt](https://github.com/firebase/php-jwt)

### HybridAuth - Open source social sign on PHP Library. HybridAuth goal is to act as an abstract api between your application and various social apis and identities providers such as Facebook, Twitter and Google.
https://github.com/hybridauth/hybridauth
https://hybridauth.github.io/

### Guzzle 6
http://docs.guzzlephp.org/en/stable/overview.html
