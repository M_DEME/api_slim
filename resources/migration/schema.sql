CREATE DATABASE IF NOT EXISTS `vitoboisxuroot` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

USE `vitoboisxuroot`;

DROP USER IF EXISTS 'vitoboisxuroot'@'%';
CREATE USER IF NOT EXISTS 'vitoboisxuroot'@'%' IDENTIFIED  WITH mysql_native_password BY '4Lt5z4QbeyC2LC39';

GRANT ALL PRIVILEGES ON vitoboisxuroot.* TO 'vitoboisxuroot'@'%';
FLUSH PRIVILEGES;

DROP TABLE IF EXISTS `client_has_address`;
DROP TABLE IF EXISTS `address`;
DROP TABLE IF EXISTS `client`;
DROP TABLE IF EXISTS `account`;

-- ----------
-- Account --
-- ----------
CREATE TABLE IF NOT EXISTS `account` (
  `account_index`              BIGINT AUTO_INCREMENT PRIMARY KEY,
  `account_email`              VARCHAR(50) NOT NULL,
  `account_avatar`             VARCHAR(255) DEFAULT '',
  `account_api_key`            VARCHAR(255) NOT NULL,
  `account_role`               VARCHAR(255) DEFAULT '',
  `account_password`           VARCHAR(255) NOT NULL,
  `account_is_blocked`         TINYINT(1) NOT NULL DEFAULT '0',
  `account_is_email_validated` TINYINT(1) NOT NULL DEFAULT '0',
  `account_creation_date`      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `account_last_update`        TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `account_delete_at`          TIMESTAMP NULL,
  CONSTRAINT `account_email_unique` UNIQUE(`account_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO account (account_email, account_api_key, account_password) VALUES ('dev@blstef.fr', '3b80f36091b60d2e59ce3f4e787dea2c', '31f7a65e315586ac198bd798b6629ce4903d0899476d5741a9f32e2e521b6a66');
INSERT INTO account (account_email, account_api_key, account_password) VALUES ('contact@heavytech.fr', '2xb45558tzn468q3i7bz268ycr63aymi', '15d10cacfbea80da77c46f0d9fbe23c12880dcdb7cd244b8d9958b38f350cc3f'); -- rt6ba2u643c7
INSERT INTO account (account_email, account_api_key, account_password) VALUES ('aghiles.manseur@heavytech.fr', 'ys24e62y9sq82wihs634752u5nk3xed4', 'df41cc5c0f927ed5a09e45ea5c8e92536c3ecabfe723392ece4b5f497aed481f'); -- p4b6v24s6wz8
INSERT INTO account (account_email, account_api_key, account_password) VALUES ('ambrejeanm@gmail.com', 'm9py6cs56e6544zj5gx8jfhk677b644v', '6cc49f8036fc1acf944e1121a0701c9ebcb5be1a31c2a8c591000a3aca52f8a3'); -- 9h23abv2jn39

-- ----------
-- Client --
-- ----------
CREATE TABLE IF NOT EXISTS `client` (
  `client_index`                BIGINT AUTO_INCREMENT PRIMARY KEY,
  `client_last_name`            VARCHAR(50) NOT NULL,
  `client_first_name`           VARCHAR(50) NOT NULL,
  `client_email`                VARCHAR(50) NOT NULL,
  `client_is_blocked`           TINYINT(1) NOT NULL DEFAULT '0',
  `client_is_email_validated`   TINYINT(1) NOT NULL DEFAULT '0',
  `client_creation_date`        TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `client_last_update`          TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `client_delete_at`            TIMESTAMP NULL,
  CONSTRAINT `client_email_unique` UNIQUE(`client_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO client (client_last_name, client_first_name, client_email) VALUE ('MANSEUR', 'Aghiles', 'contact@harryless.fr');
INSERT INTO client (client_last_name, client_first_name, client_email) VALUE ('BLANGIER', 'Stéphane', 'dev@blstef.fr');

CREATE TABLE IF NOT EXISTS `address` (
  `address_index`               BIGINT AUTO_INCREMENT PRIMARY KEY,
  `address_address1`            VARCHAR(255) NOT NULL,
  `address_address2`            VARCHAR(255) NOT NULL DEFAULT '',
  `address_zipcode`             SMALLINT(6) NOT NULL,
  `address_city`                VARCHAR(255) NOT NULL,
  `address_country`             VARCHAR(255) DEFAULT 'France',
  `address_lng`                 FLOAT(10,6) NULL,
  `address_lat`                 FLOAT(10,6) NULL,
  `address_creation_date`       TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `address_last_update`         TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `client_has_address` (
  `client_has_address_name`           VARCHAR(50) NOT NULL,
  `client_has_address_address_index`  BIGINT NOT NULL,
  `client_has_address_client_index`   BIGINT NOT NULL,
  FOREIGN KEY `client_has_address_address_idx_fk`(`client_has_address_address_index`) REFERENCES `address`(`address_index`) ON DELETE CASCADE,
  FOREIGN KEY `client_has_address_client_idx_fk`(`client_has_address_client_index`) REFERENCES `client`(`client_index`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
